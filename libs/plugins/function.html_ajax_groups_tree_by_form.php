<?php

function smarty_function_html_ajax_groups_tree_by_form($params, &$smarty)
{
   $level=0;
   $mainUrl=$params['url'];
   $mainUrl.=(substr($mainUrl, -1) == '/' ? '' : '/');
   $disp = '';
   
   $disp.= '<ul class="treeview-red level'.$level.'" id="red parent0">';
	  foreach($params['data'] as $k=>$v){
		
			$disp.='<li';
			$disp.=' ><input type="checkbox" name="grID[]" value="'.$v['ID'].'" id="groupsTreeItem"  >'.$v['name'];
			if(isset($v['child'])){
			$disp.=getChildStructureByForm($v['child'],1,$v['ID'],$mainUrl);
			}	
			$disp.='</li>';
		}
	   $disp.= '</ul>';
   
   return $disp;
}
function getChildStructureByForm($data,$level,$parentID,$mainUrl){
 

$disp = '';
 $disp.= '<ul class="level'.$level.'" id="parent'.$parentID.'">';
	foreach($data as $k=>$v){
		$itemPathArray = explode("/",$v['pathUrl']);
		$url = $mainUrl.$itemPathArray[count($itemPathArray)-1].'/';
		$disp.='<li';
		$disp.=' ><input type="checkbox" name="grID[]" value="'.$v['ID'].'" id="groupsTreeItem"  >'.$v['name'];
		if(isset($v['child'])){
			
			$disp.=getChildStructureByForm($v['child'],$level+1,$v['ID'],$mainUrl);
		}
		$disp.='</li>';	
	}
$disp.= '</ul>';
return $disp;	
}
?>
