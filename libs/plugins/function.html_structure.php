<?php

function smarty_function_html_structure($params, &$smarty)
{
   $fileTypeArray=array(
					'php'	=>'php',
					'js' 	=>'javascript',
					'css'	=>'css',
					//'html'	=>'html',
					'html'	=>'smarty',
					'xml'	=>'xml',
					);
   $disp = '';
   if(isset($params['data']['structure'])){
	   $disp.= '<ul style="display:none;">';
	   $defaultPath=$params['data']['location'];
	   foreach($params['data']['structure'] as $k=>$v){
			$disp.='<li';
			if(is_array($v)===false){
				if(is_numeric($k)){
					$fileType = substr(strrchr($v, '.'), 1);
					
					if(array_key_exists($fileType,$fileTypeArray)!==false){
						$currentFileType=$fileTypeArray[$fileType];
					}else{
						$currentFileType='plain_text';
					}
					$disp.=' class="fileL"><a  dirPath="'.$defaultPath.'" fileType="'.$currentFileType.'" class="file" href="#">'.$v.'</a>';
				}
			}else{
			$disp.=' class="folderL"><a class="folder" href="#">'.$k.'</a>';
			$dirPatch=$defaultPath.$k.'/';
			$disp.=getChildStructure($v,$dirPatch,$fileTypeArray);
			}	
			$disp.='</li>';
		}
	   $disp.= '</ul>';
   }
   return $disp;
}
function getChildStructure($data,$dirPatch,$fileTypeArray){

$disp = '';
$disp.= '<ul>';
	foreach($data as $k=>$v){
		$disp.='<li';
		if(is_array($v)===false){
			if(is_numeric($k)){
				$fileType = substr(strrchr($v, '.'), 1);
					
					if(array_key_exists($fileType,$fileTypeArray)!==false){
						$currentFileType=$fileTypeArray[$fileType];
					}else{
						$currentFileType='plain_text';
					}
				$disp.=' class="fileL"><a  dirPath="'.$dirPatch.'" fileType="'.$currentFileType.'" class="file" href="#">'.$v.'</a>';
			}
		}else{
			$disp.=' class="folderL"><a class="folder" href="#">'.$k.'</a>';
			$Patch=$dirPatch.$k.'/';
			$disp.=getChildStructure($v,$Patch,$fileTypeArray);
		}
		$disp.='</li>';	
	}
$disp.= '</ul>';
return $disp;	
}
?>
