<?php

function smarty_function_html_ajax_groups_tree($params, &$smarty)
{
   $level=0;
   $mainUrl=$params['url'];
   $mainUrl.=(substr($mainUrl, -1) == '/' ? '' : '/');
   $disp = '';
   
   $disp.= '<ul class="treeview-gray level'.$level.'" id="parent0">';
	  foreach($params['data'] as $k=>$v){
		$itemPathArray = explode("/",$v['pathUrl']);
		$url = $mainUrl.$itemPathArray[count($itemPathArray)-1].'/';
			$disp.='<li';
			$disp.=' ><a id="groupsTreeItem" groupID="'.$v['ID'].'" href="'.$url.'">'.$v['name'].'</a>';
			if(isset($v['child'])){
			$disp.=getChildStructure($v['child'],1,$v['ID'],$mainUrl);
			}	
			$disp.='</li>';
		}
	   $disp.= '</ul>';
   
   return $disp;
}
function getChildStructure($data,$level,$parentID,$mainUrl){
 

$disp = '';
 $disp.= '<ul class="level'.$level.'" id="parent'.$parentID.'">';
	foreach($data as $k=>$v){
		$itemPathArray = explode("/",$v['pathUrl']);
		$url = $mainUrl.$itemPathArray[count($itemPathArray)-1].'/';
		$disp.='<li';
		$disp.=' ><a id="groupsTreeItem" groupID="'.$v['ID'].'" href="'.$url.'">'.$v['name'].'</a>';
		if(isset($v['child'])){
			
			$disp.=getChildStructure($v['child'],$level+1,$v['ID'],$mainUrl);
		}
		$disp.='</li>';	
	}
$disp.= '</ul>';
return $disp;	
}
?>
