<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################

////// Class init App 'DKZ'.  \\\\\\
//
///////////////////////////////////////////////////////////////////
$currentUrl = '/?action=dkz/';
$currentAction = array();
$txt['exit']=$this->lang->get('txt_exit');
$txt['add']=$this->lang->get('txt_add');
$txt['edit']=$this->lang->get('txt_edit');
$txt['delete']=$this->lang->get('txt_delete');
$txt['save']=$this->lang->get('txt_save');
$txt['close']=$this->lang->get('txt_close');
$txt['saved_success']=$this->lang->get('txt_saved_success');
$txt['select_filter_options']=$this->lang->get('txt_select_filter_options');
$txt['no']=$this->lang->get('txt_no');
$txt['yes']=$this->lang->get('txt_yes');
$txt['yes']=$this->lang->get('txt_about_us');
$this->hub->set('txt',$txt);

$Parts['api']=array(	
			'class'		=>'api',
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['exit']=array(	
			'class'		=>'logout',
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['connection']=array(	
			'class'		=>'connection',
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['comments']=array(	
			'class'		=>'comments',
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['notifications']=array(	
			'class'		=>'notifications',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'boxes/list.notifi.default.html',
						'form'=>'boxes/form.notifi.default.html',
						'filter'=>false,
						'left'=>false,
						),
			'current'  	=>0,
			'display'	=>0,
			);
			
$Parts['register']=array(	
			'class'		=>'register',
			'tpl'  		=>'register.html',
			'current'  	=>0,
			'display'	=>0,
			
			);
$Parts['singin']=array(	
			'class'		=>'singin',
			'tpl'  		=>'register.html',
			'current'  	=>0,
			'display'	=>0,
			
			);
$Parts['mainpage']=array(	
			'class'		=>'mainpage',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'boxes/list.post.default.html',
						'form'=>false,
						'filter'=>false,
						'left'=>'boxes/left.col.default.html',
						),
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['post']=array(	
			'class'		=>'post',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'boxes/list.post.404.html',
						'form'=>false,
						'filter'=>false,
						'left'=>false,
						),
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['user']=array(	
			'class'		=>'users',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'profile.html',
						'form'=>false,
						'filter'=>false,
						'left'=>false,
						),
			'current'  	=>0,
			'display'	=>0,
			);

$Parts['forum']=array(	
			'class'		=>'forum',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'boxes/list.post.default.html',
						'form'=>false,
						'filter'=>false,
						'left'=>'boxes/list.users.html',
						),
			'current'  	=>0,
			'display'	=>0,
			
			);
$Parts['media']=array(	
			'class'		=>'media',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'boxes/list.post.media.html',
						'form'=>false,
						'filter'=>false,
						'left'=>'boxes/left.col.media.html',
						),
			'current'  	=>0,
			'display'	=>0,
			
			);
$Parts['shopping']=array(	
			'class'		=>'shopping',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'boxes/list.post.product.html',
						'form'=>false,
						'filter'=>'boxes/left.col.shopping.html',
						'left'=>false,
						),
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['customs']=array(	
			'class'		=>'customs',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'boxes/list.post.customs.html',
						'form'=>false,
						'filter'=>false,
						'left'=>'boxes/left.col.customs.html',
						),
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['transport']=array(	
			'class'		=>'transport',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'boxes/list.post.transport.html',
						'form'=>false,
						'filter'=>false,
						'left'=>'boxes/left.col.transport.html',
						),
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['office']=array(	
			'class'		=>'office',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'boxes/list.post.default.html',
						'form'=>'boxes/form.post.default.html',
						'filter'=>false,
						'left'=>'boxes/left.col.office.html',
						),
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['message']=array(	
			'class'		=>'message',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'dialog.html',
						'form'=>false,
						'filter'=>false,
						'left'=>'boxes/list.contacts.html',
						),
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['partners']=array(	
			'class'		=>'partners',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'followers.html',
						'form'=>false,
						'filter'=>false,
						'left'=>'boxes/list.followers.html',
						),
			'current'  	=>0,
			'display'	=>0,
			);
$Parts['settings']=array(	
			'class'		=>'settings',
			'tpl'  		=>'mainpage.html',
			'blocks'  	=>array(
						'content'=>'settings.html',
						'form'=>false,
						'filter'=>false,
						'left'=>'boxes/form.upload.avatar.html',
						),
			'current'  	=>0,
			'display'	=>0,
			);
			

if(isset($this->Action[1])){
	$action = $this->Action[1];
	if(isset($this->currentUser['id']) and $this->currentUser['level']!=99){
		if($action =='singin' OR $action =='register'){
			header('Location: /?action=dkz/mainpage');	
		}
	}
	if(array_key_exists($action,$Parts)!==false){
		$Parts[$action]['current']=1;
		$currentAction=$Parts[$action];
		$this->hub->set('currentAction',$currentAction);
		
		if(isset($Parts[$action]['class'])){
			//print_r($this->request->post); exit();
			$currentUrl.=$action.'/';
			if(isset($this->request->post['ajaxData'])){
				
				$func = $this->request->post['dataType'];
				$arg = $this->request->post['arg'];
				$this->load->controller($Parts[$action]['class'].'/'.$func,$arg);
			}else{
				$this->load->controller($Parts[$action]['class']);
			}	
		}
	}
}else{
	if(isset($this->currentUser['id']) and $this->currentUser['level']!=99){
		header('Location: /?action=dkz/mainpage');	
	}
}
if(false !==$this->cache->get('whoOnline')){
	$whoOnline = $this->cache->get('whoOnline');
	$this->Smarty->assign("whoOnline",$whoOnline);
}
if(false !==$this->cache->get('countries')){
	$countries = $this->cache->get('countries');
	$this->Smarty->assign("countries",$countries);
}
$this->Smarty->assign("txt",$this->hub->get('txt'));
$this->Smarty->assign("txt_json",json_encode($this->hub->get('txt')));
$this->Smarty->assign("currentPatch",$app['frontend']);
$this->Smarty->assign("currentAction",$this->hub->get('currentAction'));
$this->Smarty->assign("currentUser",$this->currentUser);
$this->Smarty->assign("Sections",$Parts);
?>