<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerConnection extends Controller{
	protected $fieldsArr = [
			'user_follower_events'=>array(
										'user_id',
										'follower_id',
										'autor_id',
										'event_id',
										'event_type',
										'status',
										'created_at',
										'updated_at',
										'microtime',
									),
			'user_follower_votes'=>array(
										'user_id',
										'follower_id',
										'autor_id',
										'event_id',
										'event_type',
										'vote_id',
										'status',
										'created_at',
										'updated_at',
										'microtime',
									),
			'user_follower_comments'=>array(
										'user_id',
										'follower_id',
										'autor_id',
										'event_id',
										'event_type',
										'comment_id',
										'status',
										'created_at',
										'updated_at',
										'microtime',
									),
									
		];
	public function index($arg=array()) {}
	public function add($arg=array()) {
		
		$this->load->model('partners');
		$myPartnersIDs = array();
		$myPartnersIDs[$arg['autor_id']] = $arg['autor_id'];
		$myPartnersData = $this->currentUser['myPartnersIDs'];
		$dn = new DateTime();
		if($myPartnersData){
			foreach($myPartnersData as $id=>$status){
				if($status>1){
					$myPartnersIDs[$id]=$id;
				}
				
			}
		}	
		if($myPartnersIDs){
			foreach($myPartnersIDs as $id){
				if($this->currentUser['id']!=$id){
					$this->table = $arg['table'];
					$fields = $this->fieldsArr[$arg['table']];
					$this->fields = $fields;
					$this->postData['user_id'] = $this->currentUser['id'];
					$this->postData['follower_id'] = $id;
					$this->postData['event_id'] = $arg['event_id'];
					$this->postData['autor_id'] = $arg['autor_id'];
					$this->postData['event_type'] = $arg['event_type'];
					$this->postData['created_at'] = $dn->format('Y-m-d H:i:s');
					$this->postData['microtime'] = microtime(true);
					if(isset($arg['comment_id'])){
						$this->postData['comment_id'] = $arg['comment_id'];
					}
					if(isset($arg['vote_id'])){
						$this->postData['vote_id'] = $arg['vote_id'];
					}
					$this->save('INSERT');
				}
			}
		}	
		return true;
	}
	public function del($arg=array()){
		
		if(isset($arg['comment_id'])){
			$sql = "DELETE FROM ".$arg['table']." 
						WHERE 
						comment_id=".$arg['comment_id']." 
						AND user_id=".$this->currentUser['id']." 
						";
		}else{
			$sql = "DELETE FROM ".$arg['table']." 
						WHERE 
						user_id=".$this->currentUser['id']." 
						AND event_id=".$arg['event_id']." 
						AND event_type='".$arg['event_type']."' 
						";
		}				
		$this->db->customSQL($sql);		
		return true;	
	}
	public function like($arg)
	{
		$this->load->model($arg['model']);
		$model = 'model_dkz_'.$arg['model'];
		$event = $this->$model->find($arg['id']);
		if($this->controlVote($event['id'], $arg['model'], 'l')===true){
			$event['votes_up'] = $event['votes_up']+1;
			if($this->controlVote($event['id'], $arg['model'], 'd')===false){
				$event['votes_down'] = $event['votes_down']-1;
			}
			$this->updateEvent($model,$event);
			$data = [
					'user_id'=>$this->currentUser['id'], 
					'event_id'=>$event['id'], 
					'event_type'=>$arg['model'], 
					'vote_type'=>'l', 
					
				];
			$this->db->customSQL("DELETE FROM  `event_votes` 
										WHERE event_id=".$event['id']." 
										  and event_type='".$arg['model']."' 
										  and user_id=".$this->currentUser['id']." ");	
				
			$this->del(array('table'=>'user_follower_votes','event_id'=>$event['id'],'event_type'=>$arg['model']));
			$this->table = 'event_votes';
			$this->fields = array_keys($data);
			$this->postData =$data;
			$this->save('INSERT');
			$vote_id = $this->sqlID;
			$this->add(array('table'=>'user_follower_votes','event_id'=>$event['id'],'autor_id'=>$event['user_id'],'event_type'=>$arg['model'],'vote_id'=>$vote_id));
		}else{
			$event['votes_up'] = $event['votes_up']-1;
			$this->updateEvent($model,$event);
			$this->db->customSQL("DELETE FROM  `event_votes` 
										WHERE event_id=".$event['id']." 
										  and event_type='".$arg['model']."' 
										  and user_id=".$this->currentUser['id']." ");	
			$this->del(array('table'=>'user_follower_votes','event_id'=>$event['id'],'event_type'=>$arg['model']));
		}
		echo json_encode(array('event'=>$event));
		exit();
	}
	public function dislike($arg)
	{
		$this->load->model($arg['model']);
		$model = 'model_dkz_'.$arg['model'];
		$event = $this->$model->find($arg['id']);
		if($this->controlVote($event['id'],$arg['model'], 'd')===true){
			$event['votes_down'] = $event['votes_down']+1;
			if($this->controlVote($event['id'],$arg['model'], 'l')===false){
				$event['votes_up'] = $event['votes_up']-1;
			}
			$this->updateEvent($model,$event);
			$data = [
					'user_id'=>$this->currentUser['id'], 
					'event_id'=>$event['id'], 
					'event_type'=>$arg['model'],  
					'vote_type'=>'d', 
					
				];
			$this->db->customSQL("DELETE FROM  `event_votes` 
										WHERE event_id=".$event['id']." 
										  and event_type='".$arg['model']."' 
										  and user_id=".$this->currentUser['id']." ");	
			$this->table = 'event_votes';
			$this->fields = array_keys($data);
			$this->postData =$data;
			$this->save('INSERT');
		}else{
			$event['votes_down'] = $event['votes_down']-1;
			$this->updateEvent($model,$event);
			$this->db->customSQL("DELETE FROM  `event_votes` 
										WHERE event_id=".$event['id']." 
										  and event_type='".$arg['model']."' 
										  and user_id=".$this->currentUser['id']." ");	
		}
		echo json_encode(array('event'=>$event));
		exit();
	}
	public function favorites($arg)
	{
		$this->load->model($arg['model']);
		$model = 'model_dkz_'.$arg['model'];
		$event = $this->$model->find($arg['id']);
		$dn = new DateTime();
		$color = 0;
		if($this->controlFavor($event['id'],$arg['model'])===true){
			$event['favorites'] = $event['favorites']+1;
			$this->updateEvent($model,$event);
			$data = [
					'user_id'=>$event['user_id'], 
					'subscriber_id'=>$this->currentUser['id'], 
					'event_id'=>$event['id'], 
					'event_type'=>$arg['model'],  
					'created_at'=>$dn->format('Y-m-d H:i:s'), 
					
				];
				
			$this->table = 'user_subscriber_events';
			$this->fields = array_keys($data);
			$this->postData =$data;
			$this->save('INSERT');
			$color = "gold";
		}else{
			$event['favorites'] = $event['favorites']-1;
			$this->updateEvent($model,$event);
			$this->db->customSQL("DELETE FROM  `user_subscriber_events` 
										WHERE event_id=".$event['id']." 
										  and event_type='".$arg['model']."' 
										  and subscriber_id=".$this->currentUser['id']." ");	
		}
		echo json_encode(array('event'=>$event,"color"=>$color));
		exit();
	}
	public function followDel($arg)
	{
		$sql = "DELETE FROM partners WHERE id_partner=".$arg['uid']." and id_user=".$this->currentUser['id'];
		$this->db->customSQL($sql);
		$sql = "DELETE FROM partners WHERE id_user=".$arg['uid']." and id_partner=".$this->currentUser['id'];
		$this->db->customSQL($sql);
		unset($_SESSION['currentUser']['myPartnersIDs'][$arg['uid']]);
		$this->hub->set('currentUser',$_SESSION['currentUser']);
	/*	foreach($this->fieldsArr as $table=>$fields){
			$sql = "DELETE FROM ".$table." WHERE user_id=".$arg['uid']." and follower_id=".$this->currentUser['id'];
			$this->db->customSQL($sql);
			$sql = "DELETE FROM ".$table." WHERE follower_id=".$arg['uid']." and user_id=".$this->currentUser['id'];
			$this->db->customSQL($sql);
			
		}
	*/	
		echo json_encode(array('html'=>$this->lang->get('complete'),));
		exit();
	}
	public function followConfirm($arg)
	{
		$sql = "UPDATE partners SET status=2 WHERE id_user=".$arg['uid']." and id_partner=".$this->currentUser['id'];
		$this->db->customSQL($sql);
		$_SESSION['currentUser']['myPartnersIDs'][$arg['uid']] = 2;
			$this->hub->set('currentUser',$_SESSION['currentUser']);
		echo json_encode(array('html'=>$this->lang->get('complete'),));
		exit();
	}
	public function follow($arg)
	{
		if($this->controlFollow($arg['uid'])===true){
			$data = [
					'id_partner'=>$arg['uid'], 
					'id_user'=>$this->currentUser['id'], 
					'status'=>1, 
			];
				
			$this->table = 'partners';
			$this->fields = array_keys($data);
			$this->postData =$data;
			$this->save('INSERT');
			$_SESSION['currentUser']['myPartnersIDs'][$arg['uid']] = 1;
			$this->hub->set('currentUser',$_SESSION['currentUser']);
		}
		echo json_encode(array('html'=>$this->lang->get('application sent'),));
		exit();
	}
	private function controlFollow($uid){
		if($uid==$this->currentUser['id']) return false;
		$sqlData['table'] = "partners";
		$sqlData['column'] = " count(id) as cnt ";
		$sqlData['where'] =  "(id_partner=".$uid." 
							and id_user=".$this->currentUser['id'].")
							OR (id_user=".$uid." 
							and id_partner=".$this->currentUser['id'].")";
		$Data = $this->db->get($sqlData);					
		if($Data['arr'][0]['cnt'] > 0){
			return false;
		}else{
			return true;
		}
	}
	private function controlVote($event_id,$event_type,$vote_type){
		$sqlData['table'] = "event_votes";
		$sqlData['column'] = " count(id) as cnt ";
		$sqlData['where'] =  "event_id=".$event_id." 
							and user_id=".$this->currentUser['id']." 
							and event_type='".$event_type."' 
							and vote_type='".$vote_type."' ";
		$Data = $this->db->get($sqlData);					
		if($Data['arr'][0]['cnt'] > 0){
			return false;
		}else{
			return true;
		}
	}
	private function controlFavor($event_id,$event_type){
		$sqlData['table'] = "user_subscriber_events";
		$sqlData['column'] = " count(id) as cnt ";
		$sqlData['where'] =  "event_id=".$event_id." 
							and subscriber_id=".$this->currentUser['id']." 
							and event_type='".$event_type."' 
							 ";
		$Data = $this->db->get($sqlData);					
		if($Data['arr'][0]['cnt'] > 0){
			return false;
		}else{
			return true;
		}
	}
	
	private function updateEvent($model,$event)
	{
		$this->table = $this->$model->table;
		$fields = $this->$model->getRows();
		$this->fields = $fields;
		$this->postData = $event;
		$this->save('UPDATE');
		return true;	
	}
}	