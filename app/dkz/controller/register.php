<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerRegister extends Controller{
	public $user = array();
	public $errors = array();
	public $regType = 'comp';
	
	
	public function index($arg=array()) {
		
		$currentSection = array();
		$currentPart= false;
		$currentUrl = '/?action=dkz/register/';
		$currentFormUrl = 'dkz/register/';
		$Sections = $this->getSections();
		if(isset($this->Action[2])){
			$action = $this->Action[2];
			
			if(array_key_exists($action,$Sections)!==false){
				if(isset($Sections[$action]['class'])){
					$currentUrl.=$action.'/';
					$currentFormUrl.=$action.'/';
					if(isset($this->request->post['ajaxData'])){
						$func = $this->request->post['dataType'];
						$arg = $this->request->post['arg'];
						$this->load->controller($Sections[$action]['class'].'/'.$func,$arg);
					}else{
						
						$this->load->controller($Sections[$action]['class']);
					}	
				}
				$Sections[$action]['current']=1;
				$currentSection=$Sections[$action];
				$currentPart=$Sections[$action]['currentPart'];
				
				
			}
		}else{
			$this->Smarty->assign("content",'register_form.html');		
			$this->Smarty->assign("currentPart",$currentPart);		
			$this->Smarty->assign("currentFormUrl",$currentFormUrl);		
			$this->Smarty->assign("currentUrl",$currentUrl);
			$this->Smarty->assign("user",$this->user);
			$this->Smarty->assign("errors",$this->errors);
			$this->Smarty->assign("regType",$this->regType);
		}
		
		
	}
	public function add($arg=array()){
		$this->load->model('user');
		$this->setFields();
		
		if(!isset($this->request->post['email'])){
			header("Location:/?action=dkz/register");
			exit();
		}
		$data = $this->request->post;
		$this->user[$data['type']] = $data;
		$this->regType = $data['type'];
		if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
			$this->errors[$data['type']]['email'] = 'введите правильный email';
		}
		if($data['password'] != $data['password_confirm']){
			$this->errors[$data['type']]['password'] = 'пароли не совпадают';
		}
		if(!$this->model_dkz_user->checkEmail($data['email'])){
			$this->errors[$data['type']]['email'] = 'e-mail занят';
		}
		if(empty($this->errors)){
		
			
			$data['pass'] = password_hash($data['password'], PASSWORD_DEFAULT);
			unset($data['password']);
			$this->table = $this->model_dkz_user->table;
			foreach($this->fields as $field){
				if(isset($data[$field])){
					$this->postData[$field]=$data[$field];
				}
			}
			$this->postData['join_date'] = date('Y-m-d H:i:s');
			$this->postData['fotoprof'] = 'img/gallery/nofoto.png';
			$this->save('INSERT');
			$_SESSION['currentUser'] = $this->postData;
			$_SESSION['currentUser']['id'] = $this->sqlID;
			$_SESSION['currentUser']['level'] = 99;
			$_SESSION['currentUser']['groupID'] = 99;
			$this->hub->set('currentUser',$_SESSION['currentUser']);
			header("Location:/?action=dkz/register/uploadfile");
			exit();
			
		}else{
			$this->Smarty->assign("content",'register_form.html');	
			$this->Smarty->assign("user",$this->user);
			$this->Smarty->assign("errors",$this->errors);
			$this->Smarty->assign("regType",$this->regType);
		}
		
	
		
	}
	public function uploadfile(){
		
		$this->Smarty->assign("content",'uploadfile.html');	
	}
	public function uploadprocessing(){
		$file = $this->request->files["file"];
		$id = $this->currentUser['id'];
        if ($this->isSecurity($file)){  $this->loadDoc($file, $id); 
			$res = ['answer' => 'ok'];
		}else{  
			$res = ['answer' => 'что-то не так'];
		}
		exit(json_encode($res));   
	}
	public function waitmoder(){
		
		$this->Smarty->assign("content",'waitmoder.html');	
	}
	
	private function setFields(){
		$this->load->model('user');
		$this->fields = $this->model_dkz_user->getRows();
	}
	
	private function loadDoc($file, $id){
		$type = $file['type'];
		$dir = mkdir("img/gallery/documents/".$id."/", 0777);
		$uploaddir = "img/gallery/documents/".$id."/";
		$name = $id.rand(10,1000).".".substr($type, strlen("image/"));
			$uploadfile = $uploaddir.$name;
		if (move_uploaded_file($file["tmp_name"], $uploadfile)){
			$_SESSION['msg'] = 'file loaded';
			
			return true;
		}
		else return false;
	}
	private function isSecurity($file){
		$name = $file['name'];
		$type = $file['type'];
		$size = $file['size'];
		$blacklfoto = array(".php",".phtml",".php3",".php4");
		foreach($blacklfoto as $item){
			if(preg_match("/$item\$/i", $name)) return false;
		}
		if (($type != "image/jpg")&&($type != "image/jpeg")&&($type != "image/gif")&&($type != "image/png")) return false;

		if($size > 500*1024*1024) {
			 $_SESSION['msg'] = 'Very big foto)))';
			return false;
			}
			 return true;
	}
	private function getSections(){
		
		
		$Sections['add']=array(	
			'title'		=>$this->lang->get('Registration'),
			'class'		=>'register/add',
			'currentPart'=>'register',
			);
		$Sections['uploadfile']=array(	
			'title'		=>$this->lang->get('Upload file'),
			'class'		=>'register/uploadfile',
			'tpl'  		=>'register.html',
			'currentPart'=>'register',
			);
		$Sections['uploadprocessing']=array(	
			'title'		=>$this->lang->get('Upload file'),
			'class'		=>'register/uploadprocessing',
			'tpl'  		=>'register.html',
			'currentPart'=>'register',
			);
		$Sections['waitmoder']=array(	
			'title'		=>$this->lang->get('Moderation wait'),
			'class'		=>'register/waitmoder',
			'tpl'  		=>'register.html',
			'currentPart'=>'register',
			);
		
		return $Sections;	
	}
}
?>