<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerSettings extends Controller{
	public $user = array();
	public $errors = array();
	public $regType = 'comp';
	
	
	public function index($arg=array()) {
		$currentSection = array();
		$currentPart= false;
		$currentUrl = '/?action=dkz/settings/';
		$currentFormUrl = 'dkz/settings/';
		$Sections = $this->getSections();
		if(isset($this->Action[2])){
			$action = $this->Action[2];
			
			if(array_key_exists($action,$Sections)!==false){
				if(isset($Sections[$action]['class'])){
					$currentUrl.=$action.'/';
					$currentFormUrl.=$action.'/';
					if(isset($this->request->post['ajaxData'])){
						$func = $this->request->post['dataType'];
						$arg = $this->request->post['arg'];
						$this->load->controller($Sections[$action]['class'].'/'.$func,$arg);
					}else{
						
						$this->load->controller($Sections[$action]['class']);
					}	
				}
				$Sections[$action]['current']=1;
				$currentSection=$Sections[$action];
				$currentPart=$Sections[$action]['currentPart'];
				
				
			}
		}else{
			$this->load->model('user');
			$str = 'id='.$this->currentUser['id'];
			$user = $this->model_dkz_user->getPartnerBy($str);
			$this->Smarty->assign("user",$user);
			$this->Smarty->assign("languages",$this->lang->languages);
		}
	}
	public function update($arg=array()){
		$this->load->model('user');
		$this->setFields();
		
		$data = $this->request->post;
		$this->user = $data;
		$this->regType = $data['type'];
		if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
			$this->errors['email'] = 'введите правильный email';
		}
		if(!empty($data['password'])){
			if($data['password'] != $data['password_confirm']){
				$this->errors['password'] = 'пароли не совпадают';
			}
		}
		if(!$this->model_dkz_user->checkEmail($data['email'],$this->currentUser['id'])){
			$this->errors['email'] = 'e-mail занят';
		}
		if(empty($this->errors)){
		
			if(!empty($data['password'])){
				$data['pass'] = password_hash($data['password'], PASSWORD_DEFAULT);
			}
			unset($data['password']);
			$this->table = $this->model_dkz_user->table;
			foreach($this->fields as $field){
				if(isset($data[$field])){
					$this->postData[$field]=$data[$field];
				}
			}
			$this->postData['id'] = $this->currentUser['id'];
			$this->save('UPDATE');
			$str = 'id='.$this->currentUser['id'];
			$user = $this->model_dkz_user->getPartnerBy($str);
			$user['groupID'] = $user['level'];
			$myPartnersIDs = $this->currentUser['myPartnersIDs'];
			$_SESSION['currentUser'] = $user;
			$_SESSION['currentUser']['myPartnersIDs'] = $myPartnersIDs;
			$this->hub->set('currentUser',$_SESSION['currentUser']);
			header("Location:/?action=dkz/settings");
			exit();
			
		}else{
			$this->Smarty->assign("content",'settings.html');	
			$this->Smarty->assign("user",$this->user);
			$this->Smarty->assign("errors",$this->errors);
			$this->Smarty->assign("regType",$this->regType);
		}
	}
	public function upload($arg=array()){
		
		$this->load->model('user');
		$this->setFields();
		$this->table = $this->model_dkz_user->table;
		if(isset($this->request->files['avatar'])){
			
			$id = $this->currentUser['id'];
			$file = $this->request->files['avatar'];
			if ($this->isSecurity($file)){
				$type = $file['type'];
				$uploaddir = "img/gallery/avatars/";
				$name = $id.rand(10,1000).".".substr($type, strlen("image/"));
					$uploadfile = $uploaddir.$name;
				if (move_uploaded_file($file["tmp_name"], $uploadfile)){
					$this->postData['id'] = $id;
					$this->postData['fotoprof'] = $uploadfile;
					$this->save('UPDATE');
					
				}
			}
			
		}
		$str = 'id='.$this->currentUser['id'];
		$user = $this->model_dkz_user->getPartnerBy($str);
		$user['groupID'] = $user['level'];
		$_SESSION['currentUser'] = $user;
		$this->hub->set('currentUser',$_SESSION['currentUser']);
		header("Location:/?action=dkz/settings");
		exit();
	}
	private function isSecurity($file){
		$name = $file['name'];
		$type = $file['type'];
		$size = $file['size'];
		$blacklfoto = array(".php",".phtml",".php3",".php4");
		foreach($blacklfoto as $item){
			if(preg_match("/$item\$/i", $name)) return false;
		}
		if (($type != "image/jpg")&&($type != "image/jpeg")&&($type != "image/gif")&&($type != "image/png")) return false;

		if($size > 5*1024*1024) {
			 $_SESSION['msg'] = 'Very big foto)))';
			return false;
			}
			 return true;
	}
	private function setFields(){
		$this->load->model('user');
		$this->fields = $this->model_dkz_user->getRows();
	}
	private function getSections(){
		
		
		$Sections['update']=array(	
			'title'		=>$this->lang->get('Update user settings'),
			'class'		=>'settings/update',
			'currentPart'=>'settings',
			);
		$Sections['upload']=array(	
			'title'		=>$this->lang->get('Upload user images'),
			'class'		=>'settings/upload',
			'currentPart'=>'settings',
			);
		
		
		return $Sections;	
	}
}	