<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerMainpage extends Controller{
	public function index($arg=array()) {
		
		$data = array();
		$this->load->model('mynews');
		$this->load->model('user');
		$data = $this->model_dkz_mynews->getBy(
			'access=3 or user_id='.$this->currentUser['id'].' or (access=2 and user_id in('.implode(",",array_keys($this->currentUser['myPartnersIDs'])).'))','time_news DESC');
		$autors = array();
		$autorsIDs = array();
		if($data){
			foreach($data as $item){
				$autorsIDs[$item['user_id']]=$item['user_id'];
			}
			if(count($autorsIDs)){
				$str = 'id IN('.implode(',',$autorsIDs).')';
				$tmp_ = $this->model_dkz_user->getPartnersBy($str);
				foreach($tmp_ as $t_){
					$autors[$t_['id']] = $t_;
				}
				unset($tmp_);
				unset($t_);
			}
		}
		
		$this->Smarty->assign("model","mynews");
		$this->Smarty->assign("data",$data);
		$this->Smarty->assign("autors",$autors);
	}
	
}
?>