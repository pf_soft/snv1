<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerCustoms extends Controller{
	public function index($arg=array()) {
		
		$data = array();
		$autors = array();
		$favor = array();
		
		$this->load->model('custom');
		$this->load->model('user');
		$types = array(
				'импорт',
				'экспорт',
				
				);
		if(isset($this->request->get['type']) and array_search($this->request->get['type'], $types)!==FALSE){
			$data = $this->model_dkz_custom->getBy(" (access=3 or user_id=".$this->currentUser['id']." or (access=2 and user_id in(".implode(",",array_keys($this->currentUser['myPartnersIDs']))."))) and type='".$this->request->get['type']."'",'vremya DESC');
		}else{
			$data = $this->model_dkz_custom->getBy(
			'access=3 or user_id='.$this->currentUser['id'].' or (access=2 and user_id in('.implode(",",array_keys($this->currentUser['myPartnersIDs'])).'))','vremya DESC');
		}
		
		
		$autorsIDs = array();
		if($data){
			foreach($data as $k=>$item){
				//$d = new DateTime($item['dedline']);
				//$data[$k]['deadline_format'] = $d->format('d.m.y');
				$autorsIDs[$item['user_id']]=$item['user_id'];
			}
			if(count($autorsIDs)){
				$str = 'id IN('.implode(',',$autorsIDs).')';
				$tmp_ = $this->model_dkz_user->getPartnersBy($str);
				foreach($tmp_ as $t_){
					$autors[$t_['id']] = $t_;
				}
				unset($tmp_);
				unset($t_);
			}
			$favor = $this->model_dkz_user->myFavorEvent('custom');
		}else{
			$data = array();
		}
		
		$this->Smarty->assign("model","custom");
		$this->Smarty->assign("favor",$favor);
		$this->Smarty->assign("data",$data);
		$this->Smarty->assign("autors",$autors);
	}
	
}
?>