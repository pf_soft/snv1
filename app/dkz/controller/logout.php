<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerLogout extends Controller{
	public function index($arg=array()) {
		
		unset($_SESSION['currentUser']);
		
		header('Location: /index.php');
		return true;
	}
	
}
?>