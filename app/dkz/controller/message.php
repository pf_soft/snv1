<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerMessage extends Controller{
	public function index($arg=array()) {
		$this->load->model('user');
		$this->load->model('message');
		$data = $this->model_dkz_message->getAll(' id DESC');
		$messages = array();
		$dialog = false;
		$dialoger = false;
		
		$contacts = array();
		$contactsIDs = array();
		if($data){
			foreach($data as $item){
				$fu = $item['from_user'];
				$tu = $item['to_user'];
				$hu = $this->model_dkz_message->selId($fu, $tu);
				$messages[$hu]['msg']=$item['message'];
				$messages[$hu]['s']=$item['status'];
				if($fu==$this->currentUser['id']){
					$messages[$hu]['s']=2;
				}
				$contactsIDs[$hu]=$hu;
			}
			if(count($contactsIDs)){
				$str = 'id IN('.implode(',',$contactsIDs).')';
				$tmp_ = $this->model_dkz_user->getPartnersBy($str);
				foreach($tmp_ as $t_){
					$contacts_tmp[$t_['id']] = $t_;
				}
				unset($tmp_);
				unset($t_);
				foreach($contactsIDs as $k=>$v){
					$contacts[$k]=$contacts_tmp[$k];
				}
			}
		}
		if(isset($this->request->get['to'])){
			if($this->request->get['to']==$this->currentUser['id']){
				header("Location:/?action=dkz/message");
				exit;
			}
			$to = (int)$this->request->get['to'];
			$this->unsetNewStatus($to);
			if(isset($contacts[$to])){
			$dialog = $this->model_dkz_message->showMessages($to);
			$dialoger=$contacts[$to];
			}else{
				$str = 'id ='.$to;
				$dialoger = $this->model_dkz_user->getPartnerBy($str);
				if($dialoger){
					$dialog = $this->model_dkz_message->showMessages($to);
				}
			}
		}
		$this->Smarty->assign("contacts",$contacts);
		$this->Smarty->assign("messages",$messages);
		$this->Smarty->assign("dialog",$dialog);
		$this->Smarty->assign("dialoger",$dialoger);
	}
	public function send($arg){
		$return = array();
		$return['html'] = 0;
		$this->load->model('user');
		$this->load->model('message');
		$to = (int)$arg['to'];
		
		if(!empty(trim($arg['message']) and $this->currentUser['id']!=$to)){
			$this->table = $this->model_dkz_message->table;
			$fields = $this->model_dkz_message->getRows();
			$this->fields = $fields;
			$this->postData['from_user'] = $this->currentUser['id'];
			$this->postData['to_user'] = $to;
			$this->postData['message'] = $arg['message'];
			$this->postData['status'] = 1;
			$this->postData['time_message'] = date('Y-m-d H:i:s');
			$this->save('INSERT');
		}	
			$str = 'id ='.$to;
			$dialoger= $this->model_dkz_user->getPartnerBy($str);
			$this->unsetNewStatus($to);
			$dialog = $this->model_dkz_message->showMessages($to);
			$this->Smarty->assign("dialog",$dialog);
			$this->Smarty->assign("currentUser",$this->currentUser);
			$this->Smarty->assign("dialoger",$dialoger);
			$return['html'] = $this->Smarty->fetch('boxes/list.dialog.html');
		
		echo json_encode($return);
		exit();
	}
	public function getDialog($arg){
		$return = array();
		$return['html'] = 0;
		$this->load->model('user');
		$this->load->model('message');
		$to = (int)$arg['to'];
		$str = 'id ='.$to;
		$dialoger= $this->model_dkz_user->getPartnerBy($str);
		$this->unsetNewStatus($to);
		$dialog = $this->model_dkz_message->showMessages($to);
		$this->Smarty->assign("dialog",$dialog);
		$this->Smarty->assign("currentUser",$this->currentUser);
		$this->Smarty->assign("dialoger",$dialoger);
		$return['html'] = $this->Smarty->fetch('boxes/list.dialogItem.html');
		echo json_encode($return);
		exit();
	}
	private function unsetNewStatus($to){
		$sql="UPDATE messages SET status=2 WHERE status=1 and from_user=".$to." and to_user=".$this->currentUser['id'];
		$this->db->customSQL($sql);
	}
}
?>