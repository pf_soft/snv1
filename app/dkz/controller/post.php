<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerPost extends Controller{
	protected $image = false;
	public $errors = array();
	public function index($arg=array()) {
		
		
		$events = array(
					'vizitki'	=>array('model'=>'vizitki',	'content'=>'boxes/list.post.media.html',	'form'=>false,'filter'=>false,'left'=>false),
					'product'	=>array('model'=>'product',	'content'=>'boxes/list.post.product.html',	'form'=>false,'filter'=>false,'left'=>false),
					'custom'	=>array('model'=>'custom',	'content'=>'boxes/list.post.customs.html',	'form'=>false,'filter'=>false,'left'=>false),
					
					'transport'	=>array('model'=>'transport',	'content'=>'boxes/list.post.transport.html','form'=>false,'filter'=>false,'left'=>false),
					'forumnews'	=>array('model'=>'forumnews',	'content'=>'boxes/list.post.default.html',	'form'=>false,'filter'=>false,'left'=>false),
					'mynews'	=>array('model'=>'mynews',		'content'=>'boxes/list.post.default.html',	'form'=>false,'filter'=>false,'left'=>false),
				);
		if(isset($this->request->get['model']) and isset($this->request->get['post']) and isset($events[$this->request->get['model']])){
			
			$event=$events[$this->request->get['model']];
			$model = 'model_dkz_'.$event['model'];
			$this->load->model($event['model']);
			$this->load->model('user');
			$tmp_ = $this->$model->find($this->request->get['post']);
			if($tmp_){
				if($tmp_['access']==3 
				or $this->currentUser['id']==$tmp_['id'] 
				or ($tmp_['access']==2 and isset($this->currentUser['myPartnersIDs'][$tmp_['user_id']]))){
						$currentAction = $this->hub->get('currentAction');
					if($event['content']) $currentAction['blocks']['content']=$event['content'];
					if($event['form']) $currentAction['blocks']['form']=$event['form'];
					if($event['filter']) $currentAction['blocks']['filter']=$event['filter'];
					if($event['left']) $currentAction['blocks']['left']=$event['left'];
					$autors[$tmp_['user_id']] = $this->model_dkz_user->find($tmp_['user_id']);
					$data[$tmp_['id']] = $tmp_; 
					unset($tmp_);
					$this->hub->set('currentAction',$currentAction);
					$this->Smarty->assign("model",$event['model']);
					$this->Smarty->assign("data",$data);
					$this->Smarty->assign("autors",$autors);
				}
			}
		}
	}
	public function claim($arg)
	{
		if(empty(trim($arg['text']))){
			echo json_encode(array('errors'=>'text'));
			exit();
		}
		$this->load->model($arg['model']);
		$model = 'model_dkz_'.$arg['model'];
		$event = $this->$model->find($arg['id']);
		$dn = new DateTime();
		if($arg['tp']=='post'){
		$data = [
					'autor_id'=>$event['user_id'], 
					'user_id'=>$this->currentUser['id'], 
					'event_id'=>$event['id'], 
					'event_type'=>$arg['model'],  
					'text'=>$arg['text'],  
					'created_at'=>$dn->format('Y-m-d H:i:s'), 
				];
			$this->table = 'claim_events';
		}
		if($arg['tp']=='comment'){
			$data = [
					'autor_id'=>$event['user_id'], 
					'user_id'=>$this->currentUser['id'], 
					'comment_id'=>$event['id'], 
					'text'=>$arg['text'],  
					'created_at'=>$dn->format('Y-m-d H:i:s'), 
				];
			$this->table = 'claim_comments';
		}
			$this->fields = array_keys($data);
			$this->postData =$data;
			$this->save('INSERT');
		
		echo json_encode(array('errors'=>'0'));
		exit();
	}
	public function access($arg)
	{
		$this->load->model($arg['model']);
		$model = 'model_dkz_'.$arg['model'];
		$event = $this->$model->find($arg['id']);
		if($event['access']==1 and $arg['access']!=1){
			$this->load->controller('connection/add',
								array(
									'table'=>'user_follower_events',
									'event_id'=>$event['id'] ,
									'autor_id'=>$event['user_id'],
									'event_type'=>$arg['model']
									)
							);
		}
		if($arg['access']==1 and $event['access']!=1){
			$this->load->controller('connection/del',
								array(
									'table'=>'user_follower_events',
									'event_id'=>$event['id'] ,
									'event_type'=>$arg['model']
									)
							);
		}
		$event['access'] = $arg['access'];
		$this->table = $this->$model->table;
		$fields = $this->$model->getRows();
		$this->fields = $fields;
		$this->postData = $event;
		$this->save('UPDATE');
		echo json_encode(array('errors'=>'0'));
		exit();
	}
	
}
?>