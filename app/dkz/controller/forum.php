<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerForum extends Controller{
	public function index($arg=array()) {
		
		
		$this->load->model('forumnews');
		$this->load->model('user');
		
		
		$autors = array();
		$tmp_ = $this->model_dkz_user->getAll('id DESC',false);
		foreach($tmp_ as $t_){
			$autors[$t_['id']] = $t_;
		}
		unset($tmp_);
		unset($t_);
		if(isset($this->request->get['uid'])){
			$this->load->model('partners');
			$currentAction = $this->hub->get('currentAction');
			$currentAction['blocks']['content']='profile.html';
			$this->hub->set('currentAction',$currentAction);
			
			$partnerID = (int)$this->request->get['uid'];
			
			$myPartners = array();
			$myPartnersIDs = $this->currentUser['myPartnersIDs'];
			if(count($myPartnersIDs)){
				$ids = array_keys($myPartnersIDs);
				$str = 'id IN('.implode(',',$ids).')';
				$tmp_ = $this->model_dkz_user->getPartnersBy($str);
				foreach($tmp_ as $t_){
					$myPartners[$t_['id']] = $t_;
				}
				unset($tmp_);
				unset($t_);
			}
			$this->load->model('vizitki');
			$vizitki = $this->model_dkz_vizitki->getBy('user_id='.$partnerID,'vremya DESC');
			$this->Smarty->assign("vizitki",$vizitki);
			$this->Smarty->assign("user_delails",false);
			$this->Smarty->assign("myPartnersIDs",$myPartnersIDs);
			$this->Smarty->assign("myPartners",$myPartners);
			$this->Smarty->assign("uid",$partnerID);
		}else{
			
			$data = $this->model_dkz_forumnews->getBy(
			'access=3 or user_id='.$this->currentUser['id'].' or (access=2 and user_id in('.implode(",",array_keys($this->currentUser['myPartnersIDs'])).'))','time_news DESC');
			$this->Smarty->assign("data",$data);
		}
		
		$this->Smarty->assign("model","forumnews");
		$this->Smarty->assign("autors",$autors);
	}
	
}
?>