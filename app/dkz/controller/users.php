<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerUsers extends Controller{
	public function index($arg=array()) {
		$this->load->model('user');
		$this->load->model('partners');
		$autors = array();
		
		
		if(isset($this->request->get['id'])){
			$partnerID = (int)$this->request->get['id'];
			$str = 'id ='.$partnerID;
			$profile = $this->model_dkz_user->getPartnerBy($str);
			if($profile){
				$myPartners = array();
				$myPartnersIDs = $this->currentUser['myPartnersIDs'];
				if(count($myPartnersIDs)){
					$ids = array_keys($myPartnersIDs);
					$str = 'id IN('.implode(',',$ids).')';
					$tmp_ = $this->model_dkz_user->getPartnersBy($str);
					foreach($tmp_ as $t_){
						$myPartners[$t_['id']] = $t_;
					}
					unset($tmp_);
					unset($t_);
				}
				
				$this->Smarty->assign("myPartnersIDs",$myPartnersIDs);
				$this->Smarty->assign("myPartners",$myPartners);
				$this->Smarty->assign("profile",$profile);
				$this->load->model('product');
				$this->load->model('vizitki');
				$this->load->model('custom');
				$this->load->model('transport');
				$products = $this->model_dkz_product->getBy('user_id='.$partnerID,'vremya DESC');
				$vizitki = $this->model_dkz_vizitki->getBy('user_id='.$partnerID,'vremya DESC');
				$custom = $this->model_dkz_custom->getBy('user_id='.$partnerID,'vremya DESC');
				$transport = $this->model_dkz_transport->getBy('user_id='.$partnerID,'vremya DESC');
				$autors[$partnerID] = $profile;
				
				$this->Smarty->assign("products",$products);
				$this->Smarty->assign("vizitki",$vizitki);
				$this->Smarty->assign("custom",$custom);
				$this->Smarty->assign("transport",$transport);
				$this->Smarty->assign("user_delails",true);
				$this->Smarty->assign("uid",$partnerID);
			}
			
		}
		$this->Smarty->assign("autors",$autors);
	}
}