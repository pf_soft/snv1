<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerPartners extends Controller{
	public function index($arg=array()) {
		$this->load->model('user');
		$this->load->model('partners');
		//partners
		$myPartners = array();
		$myPartnersIDs = $this->currentUser['myPartnersIDs'];
		$myPartnersIDs_tmp = array();
		if($myPartnersIDs){
			foreach($myPartnersIDs as $id=>$status){
				
				if($status==2){
					$myPartnersIDs_tmp[$id]=$id;
				}
			}
			if(count($myPartnersIDs_tmp)){
				$str = 'id IN('.implode(',',$myPartnersIDs_tmp).')';
				$tmp_ = $this->model_dkz_user->getPartnersBy($str);
				foreach($tmp_ as $t_){
					$myPartners[$t_['id']] = $t_;
				}
				unset($tmp_);
				unset($t_);
			}
		}
		
		//profi
		$mySlaves = array();
		$mySlavesIDs = array();
		$mySlavesData = $myPartnersIDs;
		if($mySlavesData){
			foreach($mySlavesData as $id=>$status){
				if($status==4){
					$mySlavesIDs[$id]=$id;
				}
			}
			if(count($mySlavesIDs)){
				$str = 'id IN('.implode(mySlavesIDs).')';
				$tmp_ = $this->model_dkz_user->getPartnersBy($str);
				foreach($tmp_ as $t_){
					$mySlaves[$t_['id']] = $t_;
				}
				unset($tmp_);
				unset($t_);
			}
		}
		if(isset($this->request->get['id'])){
			$currentAction = $this->hub->get('currentAction');
			$currentAction['blocks']['content']='profile.html';
			$this->hub->set('currentAction',$currentAction);
			$partnerID = (int)$this->request->get['id'];
			$str = 'id ='.$partnerID;
			$profile = $this->model_dkz_user->getPartnerBy($str);
			if($profile){
				$this->Smarty->assign("profile",$profile);
				$this->load->model('product');
				$this->load->model('vizitki');
				$this->load->model('custom');
				$this->load->model('transport');
				$products = $this->model_dkz_product->getBy('user_id='.$partnerID,'vremya DESC');
				$vizitki = $this->model_dkz_vizitki->getBy('user_id='.$partnerID,'vremya DESC');
				$custom = $this->model_dkz_custom->getBy('user_id='.$partnerID,'vremya DESC');
				$transport = $this->model_dkz_transport->getBy('user_id='.$partnerID,'vremya DESC');
				$autors[$partnerID] = $profile;
				$this->Smarty->assign("autors",$autors);
				$this->Smarty->assign("products",$products);
				$this->Smarty->assign("vizitki",$vizitki);
				$this->Smarty->assign("custom",$custom);
				$this->Smarty->assign("transport",$transport);
				$this->Smarty->assign("user_delails",true);
				$this->Smarty->assign("uid",$partnerID);
				
			}
			$this->Smarty->assign("partnerID",$partnerID);
		}else{
			//followers
			$myNewPartners = array();
			$myNewPartnersIDs = array();
			$myNewPartnersData = $myPartnersIDs;
			if($myNewPartnersData){
				foreach($myNewPartnersData as $id=>$status){
					if($status==1){
						$myNewPartnersIDs[$id]=$id;
					}
				}
				if(count($myNewPartnersIDs)){
					$str = 'id IN('.implode(',',$myNewPartnersIDs).')';
					$tmp_ = $this->model_dkz_user->getPartnersBy($str);
					foreach($tmp_ as $t_){
						$myNewPartners[$t_['id']] = $t_;
					}
					unset($tmp_);
					unset($t_);
				}
			}
			$this->Smarty->assign("data",$myNewPartners);
		}
		
		$this->Smarty->assign("mySlaves",$mySlaves);
		$this->Smarty->assign("myPartners",$myPartners);
		$this->Smarty->assign("myPartnersIDs",$myPartnersIDs);
	}
}