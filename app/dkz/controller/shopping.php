<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerShopping extends Controller{
	public function index($arg=array()) {
		
		$data = array();
		$this->load->model('product');
		$this->load->model('user');
		$categories_product = $this->model_dkz_product->getProductCategories();
		$str_ = [];
		$str_[] = '(access=3 or user_id='.$this->currentUser['id'].' or (access=2 and user_id in('.implode(",",array_keys($this->currentUser['myPartnersIDs'])).')))';
		if(isset($this->request->get['filter'])){
			if(isset($this->request->get['category']) and !empty($this->request->get['category'])){
				$str_[] = "category='".(int)$this->request->get['category']."'";
			}
			if(isset($this->request->get['type']) and !empty($this->request->get['type'])){
				$str_[] = "type='".$this->request->get['type']."'";
			}
			if(count($str_)){
				$str = implode(" AND ",$str_);
				$data = $this->model_dkz_product->getBy($str,'vremya DESC');
			}else{
				$data = $this->model_dkz_product->getAll('vremya DESC');
			}
			
		}else{
			$str = implode(" AND ",$str_);
			$data = $this->model_dkz_product->getBy($str,'vremya DESC');
		}
		
		
		$autors = array();
		$autorsIDs = array();
		
		if($data){
			foreach($data as $item){
				$autorsIDs[$item['user_id']]=$item['user_id'];
			}
			if(count($autorsIDs)){
				$str = 'id IN('.implode(',',$autorsIDs).')';
				$tmp_ = $this->model_dkz_user->getPartnersBy($str);
				foreach($tmp_ as $t_){
					$autors[$t_['id']] = $t_;
				}
				unset($tmp_);
				unset($t_);
			}
		}else{
			$data =array();
			}
		
		$this->Smarty->assign("categories_product",$categories_product);
		$this->Smarty->assign("data",$data);
		$this->Smarty->assign("autors",$autors);
		$this->Smarty->assign("model","product");
	}
	
}
?>