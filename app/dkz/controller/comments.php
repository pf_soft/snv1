<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerComments extends Controller{
	
	public function index($arg=array()) {}
	public function add($arg)
	{
		$this->load->model($arg['model']);
		$this->load->model('comments');
		$model = 'model_dkz_'.$arg['model'];
		$event = $this->$model->find($arg['id']);
		$dn = new DateTime();
		$data = [
				'user_id'=>$this->currentUser['id'], 
				'event_id'=>$event['id'], 
				'event_type'=>$arg['model'], 
				'text'=>$arg['text'], 
				'parent'=>0,
			];
		$this->table = $this->model_dkz_comments->table;
		$this->fields = array_keys($data);
		$this->postData =$data;
		$this->save('INSERT');
			
		
		$comment_id = $this->sqlID;
		$this->load->controller('connection/add',
					array(
					'table'=>'user_follower_comments',
					'event_id'=>$event['id'],
					'autor_id'=>$event['user_id'],
					'event_type'=>$arg['model'],
					'comment_id'=>$comment_id
					)
							);
		$event['comments'] = $event['comments']+1;
		$this->updateEvent($model,$event);
		$this->getTree(array('event_id'=>$event['id'],'event_type'=>$arg['model']));
	}
	public function getTree($arg)
	{
		$this->load->model($arg['event_type']);
		$this->load->model('comments');
		$this->load->model('user');
		$model = 'model_dkz_'.$arg['event_type'];
		$e_ = $this->$model->find($arg['event_id']);
	
		$data = $this->model_dkz_comments->getByEvent($arg);
		$tree = $data['tree'];
		$autors = array();
		if(count($data['uids'])){
			$str = 'id IN('.implode(',',$data['uids']).')';
			$tmp_ = $this->model_dkz_user->getPartnersBy($str);
			foreach($tmp_ as $t_){
				$autors[$t_['id']] = $t_;
			}
		}
		$this->Smarty->assign("tree",$tree);
		$this->Smarty->assign("autors",$autors);
		$this->Smarty->assign("event_type",$arg['event_type']);
		$this->Smarty->assign("event_id",$arg['event_id']);
		$this->Smarty->assign("currentUser",$this->currentUser);
		$return['html'] = $this->Smarty->fetch("boxes/comments.post.html");
		$return['comments_cnt'] = $e_['comments'];
		echo json_encode($return);
		exit();
	}
	public function del($arg){
		
		$_SESSION['msg'] = null;
		$this->postData = array();
		$this->fields = array();
		$this->load->model('comments');
		$comment = $this->model_dkz_comments->find($arg['id']);
		if($comment){
			$this->load->model($comment['event_type']);
			$model = 'model_dkz_'.$comment['event_type'];
			$event = $this->$model->find($comment['event_id']);
			$event['comments'] = $event['comments']-1;
			$this->updateEvent($model,$event);
			
			$this->table = $this->model_dkz_comments->table;
			$this->fields = ['id'];
			$this->postData['id'] = $comment['id'];
			$this->destroy();
			$this->load->controller('connection/del',
								array(
									'table'=>'user_follower_comments',
									'comment_id'=>$arg['id'],
									)
							);
		}
		$this->getTree(array('event_id'=>$event['id'],'event_type'=>$comment['event_type']));
	}
	private function updateEvent($model,$event)
	{
		$this->table = $this->$model->table;
		$fields = $this->$model->getRows();
		$this->fields = $fields;
		$this->postData = $event;
		$this->save('UPDATE');
		return true;	
	}
}
?>