<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerMedia extends Controller{
	public function index($arg=array()) {
		
		$data = array();
		$this->load->model('vizitki');
		$this->load->model('user');
		$types = array(
				'Коммерческие',
				'Социальные',
				'Научные',
				'Культурные',
				'Образовательные',
				'Развлекательные',
				'Информационные',
				);
		if(isset($this->request->get['type']) and array_search($this->request->get['type'], $types)!==FALSE){
			$data = $this->model_dkz_vizitki->getBy("category='".$this->request->get['type']."'",'vremya DESC');
		}else{
			$data = $this->model_dkz_vizitki->getBy(
			'access=3 or user_id='.$this->currentUser['id'].' or (access=2 and user_id in('.implode(",",array_keys($this->currentUser['myPartnersIDs'])).'))','vremya DESC');
		}
		
		$autors = array();
		$autorsIDs = array();
		if($data){
			foreach($data as $item){
				$autorsIDs[$item['user_id']]=$item['user_id'];
			}
			if(count($autorsIDs)){
				$str = 'id IN('.implode(',',$autorsIDs).')';
				$tmp_ = $this->model_dkz_user->getPartnersBy($str);
				foreach($tmp_ as $t_){
					$autors[$t_['id']] = $t_;
				}
				unset($tmp_);
				unset($t_);
			}
		}else{
			$data = array();
		}
		$this->Smarty->assign("model","vizitki");
		$this->Smarty->assign("data",$data);
		$this->Smarty->assign("autors",$autors);
	}
	
}
?>