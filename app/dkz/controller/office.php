<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerOffice extends Controller{
	protected $image = false;
	public $errors = array();
	public function index($arg=array()) {
		
		
		$events = array(
					'media'=>array('model'=>'vizitki','order'=>'vremya DESC','content'=>'boxes/list.post.media.html','form'=>'boxes/form.post.media.html','filter'=>false,'left'=>false),
					'shopping'=>array('model'=>'product','order'=>'vremya DESC','content'=>'boxes/list.post.product.html','form'=>'boxes/form.post.product.html','filter'=>false,'left'=>false),
					'customs'=>array('model'=>'custom','order'=>'vremya DESC','content'=>'boxes/list.post.customs.html','form'=>'boxes/form.post.customs.html','filter'=>false,'left'=>false),
					'transport'=>array('model'=>'transport','order'=>'vremya DESC','content'=>'boxes/list.post.transport.html','form'=>'boxes/form.post.transport.html','filter'=>false,'left'=>false),
					'forumnews'=>array('model'=>'forumnews','order'=>'time_news DESC','content'=>false,'form'=>false,'filter'=>false,'left'=>false),
					'cabinet'=>array('model'=>'mynews','order'=>'time_news DESC','content'=>false,'form'=>false,'filter'=>false,'left'=>false),
				);
		if(!isset($this->request->get['event']) or !isset($events[$this->request->get['event']])){
			$event=$events['forumnews'];
		}else{
			$event=$events[$this->request->get['event']];
		}
		$categories_product = array();
		$model = 'model_dkz_'.$event['model'];
		$this->load->model($event['model']);
		$this->load->model('user');
		$data = $this->$model->getBy('user_id='.$this->currentUser['id'],$event['order']);
		if($event['model']=='product'){
			$categories_product = $this->$model->getProductCategories();
		}
		
		$currentAction = $this->hub->get('currentAction');
		if($event['content']) $currentAction['blocks']['content']=$event['content'];
		if($event['form']) $currentAction['blocks']['form']=$event['form'];
		if($event['filter']) $currentAction['blocks']['filter']=$event['filter'];
		if($event['left']) $currentAction['blocks']['left']=$event['left'];
		$autors = array();
		$autorsIDs = array();
		if($data){
			foreach($data as $item){
				$autorsIDs[$item['user_id']]=$item['user_id'];
			}
			if(count($autorsIDs)){
				$str = 'id IN('.implode(',',$autorsIDs).')';
				$tmp_ = $this->model_dkz_user->getPartnersBy($str);
				foreach($tmp_ as $t_){
					$autors[$t_['id']] = $t_;
				}
				unset($tmp_);
				unset($t_);
			}
		}else{
			$data = array();
		}
		$this->hub->set('currentAction',$currentAction);
		$this->Smarty->assign("event",$event);
		$this->Smarty->assign("data",$data);
		$this->Smarty->assign("autors",$autors);
		$this->Smarty->assign("categories_product",$categories_product);
	}
	public function add($arg){
		//print_r($this->request->files); 
		//print_r($arg); 
		//exit();
		$_SESSION['msg'] = null;
		if(isset($arg['post_type'])){
			$this->postData = array();
			$this->fields = array();
			$func = 'add_'.$arg['post_type'];
			$this->$func($arg);
		}
		$return['errors']=0;
		if(count($this->errors)>0){
			$return['errors'] = $this->errors;
		}
		echo json_encode($return);
		exit();
	}
	public function del($arg){
		
		$_SESSION['msg'] = null;
		if(isset($arg['post_type'])){
			$this->postData = array();
			$this->fields = array();
			$this->load->model($arg['post_type']);
			$model = 'model_dkz_'.$arg['post_type'];
			$this->table = $this->$model->table;
			$fields = $this->$model->getRows();
			$this->fields = $fields;
			$this->postData['id'] = $arg['id'];
			$this->destroy();
			$this->load->model('comments');
			$sql = "DELETE FROM ".$this->model_dkz_comments->table." 
						WHERE 
						AND event_id=".$arg['id']." 
						AND event_type='".$this->$model->table."' 
						";
			$this->db->customSQL($sql);
			$this->load->controller('connection/del',
								array(
									'table'=>'user_follower_events',
									'event_id'=>$arg['id'],
									'event_type'=>$this->$model->table
									)
							);
			$this->load->controller('connection/del',
								array(
									'table'=>'user_follower_comments',
									'event_id'=>$arg['id'],
									'event_type'=>$this->$model->table
									)
							);
			$this->db->customSQL("DELETE FROM  `user_subscriber_events` 
										WHERE event_id=".$arg['id']." 
										  and event_type='".$this->$model->table."' 
										  ");				
		}
		$return['errors']=0;
		
		echo json_encode($return);
		exit();
	}
	private function add_mynews($arg){
		$this->load->model('mynews');
		$this->table = $this->model_dkz_mynews->table;
		$fields = $this->model_dkz_mynews->getRows();
		$this->fields = $fields;
		$this->postData['user_id'] = $this->currentUser['id'];
		$this->postData['news'] = $arg['news'];
		$this->postData['time_news'] = date('Y-m-d H:i:s');
		
		$this->save('INSERT');
		if(!empty($this->request->files)){
			$this->uploadImage('news',$this->sqlID);
		}
		$this->load->controller('connection/add',
								array(
									'table'=>'user_follower_events',
									'event_id'=>$this->sqlID,
									'autor_id'=>$this->currentUser['id'],
									'event_type'=>$this->model_dkz_mynews->table
									)
							);
		$this->postData = array();
		if($this->image!==FALSE){
			$this->fields = $fields;
			$this->postData['id'] = $this->sqlID;
			$this->postData['image'] = $this->image;
			$this->save('UPDATE');
			$this->image = false;
		}
		return true;
	}
	private function add_forumnews($arg){
		$this->load->model('forumnews');
		$this->table = $this->model_dkz_forumnews->table;
		$fields = $this->model_dkz_forumnews->getRows();
		$this->fields = $fields;
		$this->postData['user_id'] = $this->currentUser['id'];
		$this->postData['news'] = $arg['news'];
		$this->postData['time_news'] = date('Y-m-d H:i:s');
		
		$this->save('INSERT');
		if(!empty($this->request->files)){
			$this->uploadImage('idea',$this->sqlID);
		}
		$this->load->controller('connection/add',
								array(
									'table'=>'user_follower_events',
									'event_id'=>$this->sqlID,
									'autor_id'=>$this->currentUser['id'],
									'event_type'=>$this->model_dkz_forumnews->table
									)
							);
		$this->postData = array();
		if($this->image!==FALSE){
			$this->fields = $fields;
			$this->postData['id'] = $this->sqlID;
			$this->postData['image'] = $this->image;
			$this->save('UPDATE');
			$this->image = false;
		}
		return true;
	}
	private function add_media($arg){
		$this->load->model('vizitki');
		$this->table = $this->model_dkz_vizitki->table;
		$fields = $this->model_dkz_vizitki->getRows();
		$this->fields = $fields;
		$this->postData['user_id'] = $this->currentUser['id'];
		$this->postData['category'] = $arg['cat'];
		$this->postData['vremya'] = date('Y-m-d H:i:s');
		
		$this->save('INSERT');
		if(!empty($this->request->files)){
			$this->uploadImage('media',$this->sqlID);
		}
		$this->load->controller('connection/add',
								array(
									'table'=>'user_follower_events',
									'event_id'=>$this->sqlID,
									'autor_id'=>$this->currentUser['id'],
									'event_type'=>$this->model_dkz_vizitki->table
									)
							);
		$this->postData = array();
		if($this->image!==FALSE){
			$this->fields = $fields;
			$this->postData['id'] = $this->sqlID;
			$this->postData['vizitka'] = $this->image;
			$this->save('UPDATE');
			$this->image = false;
		}
		return true;
	}
	private function add_product($arg){
		if($arg['category'] == ''){
			$this->errors['inputcategory'] = 'enter category';
		}

		if($arg['name_seo'] == ''){
			$this->errors['inputname_seo'] = 'enter product name';
		} 
		if($arg['about'] == ''){
			$this->errors['inputabout'] = 'enter manufactured';
		}
		if($arg['price'] == ''){
			$this->errors['inputprice'] = 'enter price';
		}
		if(empty($this->errors)){
			$this->load->model('product');
			$this->table = $this->model_dkz_product->table;
			$fields = $this->model_dkz_product->getRows();
			$this->fields = $fields;
			$this->postData['user_id'] = $this->currentUser['id'];
			$this->postData['type'] = $arg['type'];
			$this->postData['category'] = $arg['category'];
			$this->postData['name_seo'] = $arg['name_seo'];
			$this->postData['about'] = $arg['about'];
			$this->postData['price'] = $arg['price'];
			$this->postData['vremya'] = date('Y-m-d H:i:s');
			
			$this->save('INSERT');
			if(!empty($this->request->files)){
				$this->uploadImage('product',$this->sqlID);
			}
			$this->load->controller('connection/add',
								array(
									'table'=>'user_follower_events',
									'event_id'=>$this->sqlID,
									'autor_id'=>$this->currentUser['id'],
									'event_type'=>$this->model_dkz_product->table
									)
								);	
			$this->postData = array();
			if($this->image!==FALSE){
				$this->fields = $fields;
				$this->postData['id'] = $this->sqlID;
				$this->postData['image'] = $this->image;
				$this->save('UPDATE');
				$this->image = false;
			}
		}
		return true;
	}
	private function add_customs($arg){
		if($arg['desc'] == ''){
			$this->errors['inputdesc'] = 'enter nature of cargo';
		}
		 if($arg['price'] == ''){
			$this->errors['inputprice'] = 'enter price';
		}
		if(empty($this->errors)){
			$this->load->model('custom');
			$this->table = $this->model_dkz_custom->table;
			$fields = $this->model_dkz_custom->getRows();
			$this->fields = $fields;
			$this->postData['user_id'] = $this->currentUser['id'];
			$this->postData['type'] = $arg['type'];
			$this->postData['description'] = $arg['desc'];
			$this->postData['vidtr'] = $arg['vidtr'];
			$this->postData['ves'] = $arg['ves'];
			$this->postData['koltr'] = 1;
			if(!empty($arg['dedline'])){
				$tmp_ = new DateTime($arg['dedline']);
				$this->postData['dedline'] = $tmp_->format('Y-m-d');
			}
			$this->postData['price'] = $arg['price'];
			$this->postData['vremya'] = date('Y-m-d H:i:s');
			
			$this->save('INSERT');
			$this->load->controller('connection/add',
								array(
									'table'=>'user_follower_events',
									'event_id'=>$this->sqlID,
									'autor_id'=>$this->currentUser['id'],
									'event_type'=>$this->model_dkz_custom->table
									)
								);
		}
		return true;
	}
	private function add_transport($arg){
		if($arg['desc'] == ''){
			$this->errors['inputdesc'] = 'enter nature of cargo';
		}
		 if($arg['price'] == ''){
			$this->errors['inputprice'] = 'enter price';
		}
		if(empty($this->errors)){
			$this->load->model('transport');
			$this->table = $this->model_dkz_transport->table;
			$fields = $this->model_dkz_transport->getRows();
			$this->fields = $fields;
			$this->postData['user_id'] = $this->currentUser['id'];
			$this->postData['type'] = $arg['type'];
			$this->postData['description'] = $arg['desc'];
			$this->postData['ves'] = $arg['ves'];
			$this->postData['otkuda'] = $arg['otkuda'];
			$this->postData['kuda'] = $arg['kuda'];
			
			if(!empty($arg['dedline'])){
				$tmp_ = new DateTime($arg['dedline']);
				$this->postData['dedline'] = $tmp_->format('Y-m-d');
			}
			$this->postData['price'] = $arg['price'];
			$this->postData['vremya'] = date('Y-m-d H:i:s');
			
			$this->save('INSERT');
			$this->load->controller('connection/add',
								array(
									'table'=>'user_follower_events',
									'event_id'=>$this->sqlID,
									'autor_id'=>$this->currentUser['id'],
									'event_type'=>$this->model_dkz_transport->table
									)
								);
		}
	}
	
	private function uploadImage($type,$id){
		$file = $this->request->files["file"];
		 if ($this->isSecurity($file)){
			switch ($type) {
				case 'news':
					$this->loadNewsFoto($file, $id);
				break;
				case 'idea':
					$this->loadIdeaFoto($file, $id);
				break;
				case 'media':
					$this->loadVizitka($file, $id);
				break;
				case 'product':
					$this->loadTovarFoto($file, $id);
				break;
				
			} 
		} 
	}
	private function isSecurity($file){
		$name = $file['name'];
		$type = $file['type'];
		$size = $file['size'];
		$blacklfoto = array(".php",".phtml",".php3",".php4");
		foreach($blacklfoto as $item){
			if(preg_match("/$item\$/i", $name)) return false;
		}
		if (($type != "image/jpg")&&($type != "image/jpeg")&&($type != "image/gif")&&($type != "image/png")) return false;

		if($size > 500*1024*1024) {
			 $_SESSION['msg'] = 'Very big foto)))';
			return false;
			}
			 return true;
	}
	private function loadNewsFoto($file, $id){
		$type = $file['type'];
		$uploaddir = "img/gallery/news/";
		$name = $id.rand(10,1000).".".substr($type, strlen("image/"));
			$uploadfile = $uploaddir.$name;


		if (move_uploaded_file($file["tmp_name"], $uploadfile)){
			$this->image = $uploadfile;
			$_SESSION['msg'] = 'file loaded';

			return true;
		}
		else return false;
	}
	private function loadIdeaFoto($file, $id){
		$type = $file['type'];
		$uploaddir = "img/gallery/ideas/";
		$name = $id.rand(10,1000).".".substr($type, strlen("image/"));
			$uploadfile = $uploaddir.$name;


		if (move_uploaded_file($file["tmp_name"], $uploadfile)){
			$this->image = $uploadfile;
			$_SESSION['msg'] = 'file loaded';

			return true;
		}
		else return false;
	}
	private function loadVizitka($file, $id){
		$type = $file['type'];
		$uploaddir = "img/gallery/vizitki/";
		$name = $id.rand(10,1000).".".substr($type, strlen("image/"));
			$uploadfile = $uploaddir.$name;
		if (move_uploaded_file($file["tmp_name"], $uploadfile)){
			$this->image = $uploadfile;
			$_SESSION['msg'] = 'фото добавлено';
			return true;
		}
		else return false;
	}
	function loadTovarFoto($file, $id){
		$type = $file['type'];
		$uploaddir = "img/gallery/tovars/";
		$name = $id.rand(10,1000).".".substr($type, strlen("image/"));
			$uploadfile = $uploaddir.$name;
		if (move_uploaded_file($file["tmp_name"], $uploadfile)){
			$this->image = $uploadfile;
			$_SESSION['msg'] = 'file loaded';
			return true;
		}
		else return false;
	}
}
?>