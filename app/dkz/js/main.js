$.fn.modal.Constructor.prototype.enforceFocus = function () {
	modal_this = this
	$(document).on('focusin.modal', function (e) {
		if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
		// add whatever conditions you need here:
		&&
		!$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
			modal_this.$element.focus()
		}
	})
};
$( document ).ready(function() {
    console.log( "ready!" );
	messageCounter();
	//startTime();
	
	$("#mainscroll a").on("click", function(e){
		 $("html, body").animate({
         scrollTop: $($(this).attr("href")).offset().top + "px"
		}, {
         duration: 500,
         easing: "swing"
		});
      return false;
	});
	$("#regContainer").on("change","select[name='country']", function(e){
		if($(this).val()=='custom'){
			$(".input_county_"+$(this).attr('tpl_reg_type')).show();
			$(".input_county_"+$(this).attr('tpl_reg_type')).prop('required',true);
		}else{
			$(".input_county_"+$(this).attr('tpl_reg_type')).removeAttr('required');
			$(".input_county_"+$(this).attr('tpl_reg_type')).hide();
			
		}
	});
	$(".fm").on("click","button#delFollowBtn", function(e){
		var dataPost = {
			ajaxData: "yep",
			dataType: "followDel", 
			arg: {"uid":$(this).attr('uid')}
		};
		$.ajax({
            url: '/?action=dkz/connection',
            type: 'post',
			dataType: "json",
            data: dataPost,
            success: function(res){
				window.location.reload();
			},
			 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
			console.log( jqXHR  );
			}
        });
	});
	$(".fm").on("click","button#confirmFollowBtn", function(e){
		var dataPost = {
			ajaxData: "yep",
			dataType: "followConfirm", 
			arg: {"uid":$(this).attr('uid')}
		};
		$.ajax({
            url: '/?action=dkz/connection',
            type: 'post',
			dataType: "json",
            data: dataPost,
            success: function(res){
				window.location.reload();
			},
			 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
			console.log( jqXHR  );
			}
        });
	});
	
	$(".fm").on("click","button#addFollowBtn", function(e){
		var dataPost = {
			ajaxData: "yep",
			dataType: "follow", 
			arg: {"uid":$(this).attr('uid')}
		};
		$.ajax({
            url: '/?action=dkz/connection',
            type: 'post',
			dataType: "json",
            data: dataPost,
            success: function(res){
				$("span#addFollowContainer").html('<p style="color:green">'+res.html+'</p>');
			},
			 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
			console.log( jqXHR  );
			}
        });
	});
	$(".fm").on("click","a#likePostBtn,a#dislikePostBtn,a#favorPostBtn", function(e){
		e.preventDefault();
		if($(this).attr('id')=="likePostBtn"){
			var dataType = 'like';
		}
		if($(this).attr('id')=="dislikePostBtn"){
			var dataType = 'dislike';
		}
		if($(this).attr('id')=="favorPostBtn"){
			var dataType = 'favorites';
		}
		var link = $(this);
		var post_id = $(this).attr("post_id");
		var post_type = $(this).attr("post_type");
		var dataPost = {
			ajaxData: "yep",
			dataType: dataType, 
			arg: {"model":post_type,"id":post_id}
		};
		$.ajax({
            url: '/?action=dkz/connection',
            type: 'post',
			dataType: "json",
            data: dataPost,
            success: function(res){
				if(dataType == 'favorites'){
					$("span#favorPostCnt"+post_id).html(res.event.favorites);
					if(res.color=='0'){
						link.attr('style','text-decoration:none');
					}else{
						link.attr('style','text-decoration:none; color:'+res.color);
					}
				}
				$("span#likePostCnt"+post_id).html(res.event.votes_up);
				$("span#dislikePostCnt"+post_id).html(res.event.votes_down);
				
			},
			 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
			console.log( jqXHR  );
			}
        });
	});
	$(".fm").on("click","a#delCommentBtn", function(e){
		e.preventDefault();
		c = confirm('You a sure?');
		if(c){
			var post_id = $(this).attr("event_id");
			var dataPost = {
				ajaxData: "yep",
				dataType: "del", 
				arg: {"id":$(this).attr('comment_id')}
			};
			 $.ajax({
				url: '/?action=dkz/comments',
				type: 'post',
				dataType: "json",
				data: dataPost,
				success: function(res){
					$("#commentsPostContainer"+post_id).html(res.html);
					$("span#commentPostCnt"+post_id).html(res.comments_cnt);
				},
				 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
				console.log( jqXHR  );
				}
			});
		}
		return false;
	});
	$(".fm").on("click","button#addCommentBtn", function(e){
		var post_id = $(this).attr("event_id");
		var post_type = $(this).attr("event_type");
		var dataPost = {
			ajaxData: "yep",
			dataType: "add", 
			arg: {"model":post_type,"id":post_id,"text":$("#commentText"+post_id).val()}
		};
		$.ajax({
            url: '/?action=dkz/comments',
            type: 'post',
			dataType: "json",
            data: dataPost,
            success: function(res){
				$("#commentsPostContainer"+post_id).html(res.html);
				$("span#commentPostCnt"+post_id).html(res.comments_cnt);
			},
			 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
			console.log( jqXHR  );
			}
        });
	});
	$(".fm").on("click","a#commentPostBtn", function(e){
		e.preventDefault();
		link = $(this);
		$("#commentsPostContainer"+$(this).attr('post_id')).toggle('slow', function() {
			if($(this).is(':hidden')) { 
				$(this).html('');
			}else {
				var post_id = link.attr("post_id");
				var post_type = link.attr("post_type");
				var dataPost = {
					ajaxData: "yep",
					dataType: "getTree", 
					arg: {"event_type":post_type,"event_id":post_id}
				};
				$.ajax({
					url: '/?action=dkz/comments',
					type: 'post',
					dataType: "json",
					data: dataPost,
					success: function(res){
						$("#commentsPostContainer"+post_id).html(res.html);
						$("span#commentPostCnt"+post_id).html(res.comments_cnt);
						
					},
					 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
					console.log( jqXHR  );
					}
				});
			}
		});
	
	});
	$(".fm").on("change","input#customFile", function(e){
		$(".custom-file-label").html($(this).val());
	});
	$(".fj").on("change","input#customFile", function(e){
		$(".custom-file-label").html($(this).val());
	});
	$(".fj").on("click","a#getDialog", function(e){
		e.preventDefault();
		to = $(this).attr('to');
		urlhref=$(this).attr('href');
		message = "";
		var dataPost = {
			ajaxData: "yep",
			dataType: "send", 
			arg: {"message":message,"to":to}
		};
		 $.ajax({
            url: '/?action=dkz/message',
            type: 'post',
			dataType: "json",
            data: dataPost,
            success: function(res){
				if(res.html!=0){
				$("#dialogContainer").html(res.html);
				$("#badge-isset-new-msg"+to).hide();
				history.pushState(null, null, urlhref);
				}
			},
			 error: function(jqXHR,textStatus){
				 location.href=urlhref
				
			console.log( jqXHR  );
			}
        });
	});
	$(".fm").on("click","button#sendMessageBtn", function(e){
		message = $("#sendMessageContent").val();
		to = $(this).attr('dialoger');
		var dataPost = {
			ajaxData: "yep",
			dataType: "send", 
			arg: {"message":message,"to":to}
		};
		 $.ajax({
            url: '/?action=dkz/message',
            type: 'post',
			dataType: "json",
            data: dataPost,
            success: function(res){
				if(res.html!=0){
				$("#dialogContainer").html(res.html);
				}
			},
			 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
			console.log( jqXHR  );
			}
        });
		
	});
	
	
	$(".fm").on("click","a#claimModal,a#accesModal", function(e){
		e.preventDefault();
		$('#linksDD'+$(this).attr('item_id')).collapse('hide');
		$('#'+$(this).attr('id')+'Btn').attr('item_id',$(this).attr('item_id'));
		$('#'+$(this).attr('id')+'Btn').attr('post_type',$(this).attr('post_type'));
		$('#'+$(this).attr('id')+'Btn').attr('tp',$(this).attr('tp'));
		if($(this).attr('id')=='accesModal'){
			$('#accesModal [name=access][value="'+$(this).attr('access')+'"]').prop('checked',true);
		}
		if($(this).attr('id')=='claimModal'){
			$("#claimModal #yepContainer").hide();
			$("#claimModal #formContainer").show();
			$("#claimModal #claimModalBtn").show();
		}
		
		$('#'+$(this).attr('id')).modal('show');
	});
	$("#claimModal").on("click","button#claimModalBtn", function(e){
		e.preventDefault();
		
			var dataPost = {
				ajaxData: "yep",
				dataType: "claim", 
				arg: {"model":$(this).attr('post_type'),"id":$(this).attr('item_id'),"tp":$(this).attr('tp'),"text":$("#claimModal #inputClaim").val()}
			};
			 $.ajax({
				url: '/?action=dkz/post',
				type: 'post',
				dataType: "json",
				data: dataPost,
				success: function(res){
					if(res.errors=='0'){
					$("#claimModal #claimModalBtn").hide();
					$("#claimModal .modal-body #formContainer").hide();
					$("#claimModal .modal-body #yepContainer").show();
					$("#claimModal #inputClaim").val('');
					}else{
						$("#claimModal #inputClaim").focus();
					}
				},
				 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
				console.log( jqXHR  );
				}
			});
		return false;
	});
	$("#accesModal").on("click","button#accesModalBtn", function(e){
		e.preventDefault();
		
			var dataPost = {
				ajaxData: "yep",
				dataType: "access", 
				arg: {"model":$(this).attr('post_type'),"id":$(this).attr('item_id'),"access":$("input[name='access']:checked").val()}
			};
			 $.ajax({
				url: '/?action=dkz/post',
				type: 'post',
				dataType: "json",
				data: dataPost,
				success: function(res){
					if(res.errors=='0'){
						window.location.reload();
					}
				},
				 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
				console.log( jqXHR  );
				}
			});
		return false;
	});
	$(".fm").on("click","a#delPostBtn", function(e){
		$('#linksDD'+$(this).attr('item_id')).collapse('hide');
		e.preventDefault();
		c = confirm('You a sure?');
		if(c){
			var dataPost = {
				ajaxData: "yep",
				dataType: "del", 
				arg: {"post_type":$(this).attr('post_type'),"id":$(this).attr('item_id')}
			};
			 $.ajax({
				url: '/?action=dkz/office',
				type: 'post',
				dataType: "json",
				data: dataPost,
				success: function(res){
					if(res.errors=='0'){
						window.location.reload();
					}
				},
				 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
				console.log( jqXHR  );
				}
			});
		}
		return false;
	});
	$(".fm").on("click","button#sendPostBtn", function(e){
		
		fd = new FormData();
		post_type = $(this).attr('post_type');
		fd.append('arg[post_type]',$(this).attr('post_type'));
		$('#sendPostForm input').each(function(i){
			if($(this).attr('type')!='file')
			fd.append('arg['+$(this).attr('name')+']',$(this).val());
		});
		$('#sendPostForm select').each(function(i){
			fd.append('arg['+$(this).attr('name')+']',$(this).val());
		});
		$('#sendPostForm textarea').each(function(i){
			fd.append('arg['+$(this).attr('name')+']',$(this).val());
		});
		if(post_type!='customs' && post_type!='transport'){
			var files = $('#customFile')[0].files[0];
			fd.append('file',files);
		}
		fd.append('ajaxData','yep');
		fd.append('dataType','add');
		
		 $.ajax({
            url: '/?action=dkz/office',
            type: 'post',
			dataType: "json",
            data: fd,
            contentType: false,
            processData: false,
            success: function(res){
				if(res.errors=='0'){
					window.location.reload();
				}else{
					$.each(res.errors, function(k, v) {
						var err = 'err_'+k;
						$('#'+err).html(v);
						$('#'+k).addClass('is-invalid');
					});
				}
			},
			 error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
			console.log( jqXHR  );
			}
        });
		
	});
	$(window).scroll(function () {if ($(this).scrollTop() > 0) {$('#scroller').fadeIn();} else {$('#scroller').fadeOut();}});
	$('#scroller').click(function () {$('body,html').animate({scrollTop: 0}, 400); return false;});
});	

function messageCounter()
{
	if(UID!='0'){
		
		var dataPost = {
			ajaxData: "yep",
			dataType: "message_counter", 
			arg: {"uid":UID}
		};
		 $.ajax({
            url: '/sys/user.requests.php',
            type: 'post',
			dataType: "json",
            data: dataPost,
            success: function(res){
				if(res.m_cnt!='0'){
					$(".badge-counter").show();
					$(".badge-counter").html(res.m_cnt);
				}else{
					$(".badge-counter").hide();
				}
				if(res.n_cnt!='0'){
					$(".badge-counter-notifi").show();
					$(".badge-counter-notifi").html(res.n_cnt);
				}else{
					$(".badge-counter-notifi").hide();
				}
				
			},
			 error: function(jqXHR,textStatus){
			console.log( jqXHR  );
			}
        });
	}
	t=setTimeout('messageCounter()',5000);
}
function startTime()
{
var tm=new Date();
var h=tm.getHours();
var m=tm.getMinutes();
var s=tm.getSeconds();
m=checkTime(m);
s=checkTime(s);
document.getElementById('clock').innerHTML=h+":"+m+":"+s;
t=setTimeout('startTime()',500);
}
function checkTime(i)
{
if (i<10)
{
i="0" + i;
}
return i;
}
function randomNumberFromRange(min,max){
var randomNumber = Math.floor(Math.random()*(max-min+1)+min);
return randomNumber;
}
 function ckCreate(name)
{
	if(CKEDITOR.instances[name] === undefined) 
	{
		CKEDITOR.inline(name);
		
	}
}
var $input=$('<div class="modal-body"><input type="text" class="form-control" placeholder="Message"></div>');$(document).on("click",".js-msgGroup",function(){$(".js-msgGroup, .js-newMsg").addClass("d-none"),$(".js-conversation").removeClass("d-none"),$(".modal-title").html('<a href="#" class="js-gotoMsgs">Back</a>'),$input.insertBefore(".js-modalBody")}),$(function(){function o(){return $('[data-toggle="popover"]').length?$(window).width()-($('[data-toggle="popover"]').offset().left+$('[data-toggle="popover"]').outerWidth()):0}$(window).on("resize",function(){var t=$('[data-toggle="popover"]').data("bs.popover");t&&(t.config.viewport.padding=o())}),$('[data-toggle="popover"]').popover({template:'<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-body popover-content px-0"></div></div>',title:"",html:!0,trigger:"manual",placement:"bottom",viewport:{selector:"body",padding:o()},content:function(){var o=$("#js-popoverContent").clone();return'<ul class="nav nav-pills nav-stacked flex-column" style="width: 120px">'+o.html()+"</ul>"}}),$('[data-toggle="popover"]').on("click",function(o){o.stopPropagation(),$($('[data-toggle="popover"]').data("bs.popover").getTipElement()).hasClass("in")?($('[data-toggle="popover"]').popover("hide"),$(document).off("click.app.popover")):($('[data-toggle="popover"]').popover("show"),setTimeout(function(){$(document).one("click.app.popover",function(){$('[data-toggle="popover"]').popover("hide")})},1))})}),$(document).on("click",".js-gotoMsgs",function(){$input.remove(),$(".js-conversation").addClass("d-none"),$(".js-msgGroup, .js-newMsg").removeClass("d-none"),$(".modal-title").html("Messages")}),$(document).on("click","[data-action=growl]",function(o){o.preventDefault(),$("#app-growl").append('<div class="alert alert-dark alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Click the x on the upper right to dismiss this little thing. Or click growl again to show more growls</div>')}),$(document).on("focus",'[data-action="grow"]',function(){$(window).width()>1e3&&$(this).animate({width:300})}),$(document).on("blur",'[data-action="grow"]',function(){if($(window).width()>1e3){$(this).animate({width:180})}}),$(function(){function o(){$(window).scrollTop()>$(window).height()?$(".docs-top").fadeIn():$(".docs-top").fadeOut()}$(".docs-top").length&&(o(),$(window).on("scroll",o))}),$(function(){function o(){a.width()>768?e():t()}function t(){a.off("resize.theme.nav"),a.off("scroll.theme.nav"),n.css({position:"",left:"",top:""})}function e(){function o(){e.containerTop=$(".docs-content").offset().top-40,e.containerRight=$(".docs-content").offset().left+$(".docs-content").width()+45,t()}function t(){var o=a.scrollTop(),t=Math.max(o-e.containerTop,0);return t?void n.css({position:"fixed",left:e.containerRight,top:40}):($(n.find("li a")[1]).addClass("active"),n.css({position:"",left:"",top:""}))}var e={};o(),$(window).on("resize.theme.nav",o).on("scroll.theme.nav",t),$("body").scrollspy({target:"#markdown-toc"}),setTimeout(function(){$("body").scrollspy("refresh")},1e3)}var n=$("#markdown-toc");$("#markdown-toc li").addClass("nav-item"),$("#markdown-toc li > a").addClass("nav-link"),$("#markdown-toc li > ul").addClass("nav");var a=$(window);n[0]&&(o(),a.on("resize",o))});
// Create Base64 Object
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
