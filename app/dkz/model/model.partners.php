<?
class ModelPartners extends Model {
	public $table = 'partners';
	
	public function getMy($status){
		$ya = $_SESSION['currentUser']['id'];
		$return = false;
		$sqlData['table'] = $this->table;
		$sqlData['column'] = " * ";
		if(is_array($status)){
			$sqlData['where'] =  ' (id_user = '.$ya.' or id_partner = '.$ya.') AND status IN ('.implode(",",$status).') ORDER BY id DESC ';
		}else{
			if($status==1){
				$sqlData['where'] =  ' id_partner = '.$ya.' AND status = '.$status.' ORDER BY id DESC ';
			}else{
				$sqlData['where'] =  ' id_user = '.$ya.' AND status = '.$status.' OR id_partner = '.$ya.' AND status = '.$status.' ORDER BY id DESC ';	
			}
		}
		
		$Data = $this->db->get($sqlData);
		if(count($Data['arr'])>0)
			$return=$Data['arr'];
			
		return $return;
	}
}
?>