<?
class ModelNotifications extends Model {
	
	protected $user_ids = [];
	protected $event_ids = [];
	protected $comment_ids = [];
	protected $notifi_ids = [];
	public function getAll($order,$limit=30,$offset=false)
	{
		$this->load->model('user');
		$return = array();
		$notifications = [];
		$events = $this->getEvents($order,$limit,$offset);
		$comments = $this->getComments($order,$limit,$offset);
		$votes = $this->getVotes($order,$limit,$offset);
		$users = [];
		$eventsData = [];
		if(count($this->user_ids)){
			$str = 'id IN('.implode(',',$this->user_ids).')';
				$tmp_ = $this->model_dkz_user->getPartnersBy($str);
				foreach($tmp_ as $t_){
					$users[$t_['id']] = $t_;
				}
				unset($tmp_);
				unset($t_);
		}
		if(count($this->event_ids)){
			foreach($this->event_ids as $model_str=>$items){
				$this->load->model($model_str);
				$model = 'model_dkz_'.$model_str;
				$str = 'id IN('.implode(',',$items).')';
				$tmp_ = $this->$model->getBy($str,'id',false);
				foreach($tmp_ as $t_){
					$eventsData[$model_str][$t_['id']] = $t_;
				}
			}
		}
		$dn = new DateTime('NOW');
		foreach($events as $item){
			$d = new DateTime($item['created_at']);
			if($item['microtime']==0){
				$notifi_id=$d->getTimestamp().'.'.$item['id'];
			}else{
				$notifi_id=$item['microtime'];
			}
			$notifications[$notifi_id]['action'] = $this->lang->get('created '.$item['event_type']);
			$notifications[$notifi_id]['link'] = '/?action=dkz/post/?type='.$item['event_type'].'&id='.$item['event_id'];
			$notifications[$notifi_id]['event'] = $eventsData[$item['event_type']][$item['event_id']];
			$notifications[$notifi_id]['event_type'] = $item['event_type'];
			$notifications[$notifi_id]['date'] = $this->setDate($d,$dn);
			$notifications[$notifi_id]['who']['id'] = $item['user_id'];
			$notifications[$notifi_id]['who']['link'] = '/?action=dkz/user&id='.$item['user_id'];
			$notifications[$notifi_id]['who']['name'] = $users[$item['user_id']]['first_name'].' '.$users[$item['user_id']]['last_name'];
		}
		foreach($comments as $item){
			$d = new DateTime($item['created_at']);
			if($item['microtime']==0){
				$notifi_id=$d->getTimestamp().'.'.$item['id'];
			}else{
				$notifi_id=$item['microtime'];
			}
			$notifications[$notifi_id]['action'] = $this->lang->get('add comment to '.$item['event_type']);
			if($item['autor_id']==$this->currentUser['id']){
				$notifications[$notifi_id]['action'] = $this->lang->get('add comment to you '.$item['event_type']);
			}
			$notifications[$notifi_id]['link'] = '/?action=dkz/post/?type='.$item['event_type'].'&id='.$item['event_id'];
			$notifications[$notifi_id]['event'] = $eventsData[$item['event_type']][$item['event_id']];
			$notifications[$notifi_id]['event_type'] = $item['event_type'];
			$notifications[$notifi_id]['date'] = $this->setDate($d,$dn);
			$notifications[$notifi_id]['who']['id'] = $item['user_id'];
			$notifications[$notifi_id]['who']['link'] = '/?action=dkz/user&id='.$item['user_id'];
			$notifications[$notifi_id]['who']['name'] = $users[$item['user_id']]['first_name'].' '.$users[$item['user_id']]['last_name'];
		}
		foreach($votes as $item){
			$d = new DateTime($item['created_at']);
			if($item['microtime']==0){
				$notifi_id=$d->getTimestamp().'.'.$item['id'];
			}else{
				$notifi_id=$item['microtime'];
			}
			$notifications[$notifi_id]['action'] = $this->lang->get('liked '.$item['event_type']);
			if($item['autor_id']==$this->currentUser['id']){
				$notifications[$notifi_id]['action'] = $this->lang->get('liked you '.$item['event_type']);
			}
			$notifications[$notifi_id]['link'] = '/?action=dkz/post/?type='.$item['event_type'].'&id='.$item['event_id'];
			$notifications[$notifi_id]['event'] = $eventsData[$item['event_type']][$item['event_id']];
			$notifications[$notifi_id]['event_type'] = $item['event_type'];
			$notifications[$notifi_id]['date'] = $this->setDate($d,$dn);
			$notifications[$notifi_id]['who']['id'] = $item['user_id'];
			$notifications[$notifi_id]['who']['link'] = '/?action=dkz/user&id='.$item['user_id'];
			$notifications[$notifi_id]['who']['name'] = $users[$item['user_id']]['first_name'].' '.$users[$item['user_id']]['last_name'];
		}
		asort($notifications);
		$this->changeStatus();
		return $notifications;
	}
	/*
	public function getCount()
	{
		$return = array();
		$sqlData['table'] = "( SELECT COUNT(id) AS cnt 
					FROM user_follower_events where 
					(follower_id=".$this->currentUser['id']." or autor_id=".$this->currentUser['id'].") 
					and status=1 
					UNION 
					SELECT COUNT(id) AS cnt 
					FROM user_follower_votes where 
					(follower_id=".$this->currentUser['id']." or autor_id=".$this->currentUser['id'].") 
					and status=1 
					UNION 
					SELECT COUNT(id) AS cnt 
					FROM user_follower_comments where 
					(follower_id=".$this->currentUser['id']." or autor_id=".$this->currentUser['id'].") and status=1 ) T1 	
			";
		$sqlData['column'] = " SUM( T1.cnt ) as total_cnt ";
		$sqlData['where'] =  " 1 ";
		
		
		$Data = $this->db->get($sqlData,false);
		$return=$Data['arr'][0]['total_cnt'];
		return $return;	
	}
	*/
	public function getEvents($order,$limit=30,$offset=false)
	{
		$return = array();
		$sqlData['table'] = "user_follower_events";
		$sqlData['column'] = " * ";
		$sqlData['where'] =  " follower_id=".$this->currentUser['id'];
		$sqlData['where'].=  ' ORDER BY '.$order;
		if($limit)
			$sqlData['where'].=  ' LIMIT '.$limit;
		if($offset)
			$sqlData['where'].=  ' OFFSET '.$offset;
		$Data = $this->db->get($sqlData);
		if(count($Data['arr'])>0)
			$return=$Data['arr'];
		foreach($Data['arr'] as $item){
			$this->user_ids[$item['user_id']]=$item['user_id'];
			$this->event_ids[$item['event_type']][$item['event_id']]=$item['event_id'];
			$this->notifi_ids[$sqlData['table']][$item['id']]=$item['id'];
		}
		return $return;	
	}
	public function getComments($order,$limit=30,$offset=false)
	{
		$return = array();
		$sqlData['table'] = "user_follower_comments";
		$sqlData['column'] = " * ";
		$sqlData['where'] =  " follower_id=".$this->currentUser['id'];
		$sqlData['where'].=  ' ORDER BY '.$order;
		if($limit)
			$sqlData['where'].=  ' LIMIT '.$limit;
		if($offset)
			$sqlData['where'].=  ' OFFSET '.$offset;
		$Data = $this->db->get($sqlData);
		if(count($Data['arr'])>0)
			$return=$Data['arr'];
		foreach($Data['arr'] as $item){
			$this->user_ids[$item['user_id']]=$item['user_id'];
			$this->event_ids[$item['event_type']][$item['event_id']]=$item['event_id'];
			$this->comment_ids[$item['comment_id']]=$item['comment_id'];
			$this->notifi_ids[$sqlData['table']][$item['id']]=$item['id'];
		}
		return $return;		
	}
	public function getVotes($order,$limit=30,$offset=false)
	{
		$return = array();
		$sqlData['table'] = "user_follower_votes";
		$sqlData['column'] = " * ";
		$sqlData['where'] =  " follower_id=".$this->currentUser['id'];
		$sqlData['where'].=  ' ORDER BY '.$order;
		if($limit)
			$sqlData['where'].=  ' LIMIT '.$limit;
		if($offset)
			$sqlData['where'].=  ' OFFSET '.$offset;
		$Data = $this->db->get($sqlData);
		if(count($Data['arr'])>0)
			$return=$Data['arr'];
		foreach($Data['arr'] as $item){
			$this->user_ids[$item['user_id']]=$item['user_id'];
			$this->event_ids[$item['event_type']][$item['event_id']]=$item['event_id'];
			$this->notifi_ids[$sqlData['table']][$item['id']]=$item['id'];
		}
		return $return;		
	}
	private function changeStatus()
	{
		if(count($this->notifi_ids)>0){
			foreach($this->notifi_ids as $table=>$ids){
				if(count($ids)>0){
					$sql = "UPDATE `".$table."` SET status=0 WHERE id IN(".implode(",",$ids).")";
					$this->db->customSQL($sql);
				}
			}
		}
		return true;
	}
	private function setDate($d,$dn)
	{
		$date = $d->format('M d, Y');
		$after = $dn->getTimestamp() - $d->getTimestamp();
		$datetime1 = date_create($d->format('Y-m-d H:i:s'));
		$datetime2 = date_create($dn->format('Y-m-d H:i:s'));
		if($after<(24*60*60)){
			$interval = date_diff($datetime1, $datetime2);
			$date = $interval->format('%h hours');
			if($after<(1*60*60)){
				$date = $interval->format('%i min');
			}
			if($after<(1*60)){
				$date = $interval->format('%s sec');
			}
		}
		return $date;
	}
}
?>