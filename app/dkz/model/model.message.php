<?
class ModelMessage extends Model {
	public $table = 'messages';
	public function getAll($order,$limit=30,$offset=false){
		$return = false;
		$id=$this->currentUser['id'];
		$sqlData['table'] = $this->table;
		$sqlData['column'] = " * ";
		$sqlData['where'] =  ' id IN (
			SELECT max(id) as id FROM
		(SELECT id, from_user, to_user FROM `messages` WHERE from_user ='.$id.'
					UNION ALL
				SELECT id, to_user, from_user FROM `messages` WHERE to_user ='.$id.') t
			GROUP BY from_user, to_user
		) ';
		$sqlData['where'].=  ' ORDER BY '.$order;
		
		$Data = $this->db->get($sqlData);
		if(count($Data['arr'])>0)
			$return=$Data['arr'];
			
		return $return;
	}
	
	public function showMessages($to_who){
		$return = false;
		$otkogo=$this->currentUser['id'];
		$sqlData['table'] = $this->table;
		$sqlData['column'] = " * ";
		$sqlData['where'] =  ' from_user = '.$otkogo.' AND to_user = '.$to_who.' OR from_user = '.$to_who.' AND to_user = '.$otkogo.'  ';
		$sqlData['where'].=  ' ORDER BY time_message ASC';
		
		$Data = $this->db->get($sqlData);
		if(count($Data['arr'])>0)
			$return=$Data['arr'];
			
		return $return;
	}
	public function selId($fu,$tu){
		if($fu!=$this->currentUser['id']){
			return $fu;
		}
		else{return $tu;}
	}
}
?>