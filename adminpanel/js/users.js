$( document ).ready(function() {
    console.log( "admin.users ready!" );
	$("#contentAdminUsers").on("click", "button#addAAdminUsersBtn", function(e) {
		var arg = {
			"type": "add"
			};
		creatAdminUsersForm(arg);
	});
	$("#contentAdminUsers").on("click", "a#editAdminUsers", function(e) {
		var arg = {
			"id": $(this).attr('userID'),
			"type": "edit"
			};
		creatAdminUsersForm(arg);
		e.preventDefault();
	});
	$("#contentAdminUsers").on("click", "a#delAdminUsers", function(e) {
		var arg = {
			"id": $(this).attr('userID'),
			"deleted": 1
			};
		var dataPost = {
				ajaxAdminpanel: "yep",
				dataType: "deleteUser", 
				arg:arg
			};	
		deleteUser(dataPost);
		e.preventDefault();
	});
	
	
	$("#modalAdminUsers").on("click", "button#saveAdminUsersBtn", function(e) {
		var id = $("#modalAdminUsers input[name='user_id']").val(); 
		var type = $("#modalAdminUsers input[name='form_type']").val();
		
		var first_name = $("#modalAdminUsers input[name='first_name']").val();
		var last_name = $("#modalAdminUsers input[name='last_name']").val();
		var email = $("#modalAdminUsers input[name='email']").val();
		
		
		var level = $("#modalAdminUsers select[name='level']").val();
		
		if(type=='edit'){
			var pass = 'false';
			
		}
		
		var arg = {
				"first_name":first_name,
				"last_name":last_name,
				"email":email,
				"level":level,
				"id":id
			};
			
		var dataPost = {
				ajaxAdminpanel: "yep",
				dataType: "saveForm", 
				arg:arg
			};
		saveAdminProjectForm(dataPost);	
	});
	$('#modalAdminUsers').on('hidden.bs.modal', function (e) {
		$("#modalAdminUsers .modal-body").html('');
		$("#modalAdminUsers .modal-title").html('');
	});
});	
function deleteUser(dataPost){
	$.ajax({
			  type: "POST",
			  dataType: "json",
			  url: '/?action=adminpanel/users/',
			  data: dataPost,
			  success: function(project){
				if(project.item=='error'){
					alert(project.content);
				}else{
					window.location.reload();
				}
			  },
			  error: function(jqXHR,textStatus){
				alert( "Request failed: " + textStatus);
				console.log( jqXHR  );
				}
			});	
		return true;
}
function saveAdminProjectForm(dataPost){
	$.ajax({
			  type: "POST",
			  dataType: "json",
			  url: '/?action=adminpanel/users/',
			  data: dataPost,
			  success: function(project){
				if(project.item=='error'){
					alert(project.content);
				}else{
					window.location.reload();
				}
			  },
			  error: function(jqXHR,textStatus){
				alert( "Request failed: " + textStatus);
				console.log( jqXHR  );
				}
			});	
		return true;
}
function creatAdminUsersForm(arg){
	
	if(arg.type=='edit'){
		var dataPost = {
		ajaxAdminpanel: "yep",
		dataType: "creatForm", 
		arg: {"id":arg.id}
		};
		$.ajax({
			  type: "POST",
			  dataType: "json",
			  url: '/?action=adminpanel/users/',
			  data: dataPost,
			  success: function(project){
					var html = '';
					if(project.item=='yep'){
						var title = txt.edit_user_title;
						html = getAdminUsersForm(project.data,"edit");
					}else{
					  html = 'Ошибка! перезагрузите страницу и попробуйте еще раз.';	
					}
					$("#modalAdminUsers .modal-body").html(html);
					$("#modalAdminUsers .modal-title").html(title);
					
				},
			  error: function(jqXHR,textStatus){
				alert( "Request failed: " + textStatus);
				console.log( jqXHR  );
				}
			});	
		return true;
	}
}
function getAdminUsersForm(data,type){
	var html = '';
	var role = usersRole;
 html=html+'<form class="form-horizontal" role="form">';
	html=html+' <input type="hidden" name="user_id" value="'+data.id+'" id="input_user_id">';
	html=html+' <input type="hidden" name="form_type" value="'+type+'" id="input_form_type">';
		
		html=html+'<div class="form-group">';
			html=html+'<label for="input_firstname" class="col-sm-2 control-label">'+txt.user_fname+':</label>';
			html=html+'<div class="col-sm-10">';
				html=html+'<input type="text" class="form-control" name="first_name" value="'+data.first_name+'" id="input_firstname" placeholder="'+txt.user_fname_placeholder+'">';
			html=html+'</div>';
		html=html+'</div>';
		
		html=html+'<div class="form-group">';
			html=html+'<label for="input_lastname" class="col-sm-2 control-label">'+txt.user_lname+':</label>';
			html=html+'<div class="col-sm-10">';
				html=html+'<input type="text" class="form-control" name="last_name" value="'+data.last_name+'" id="input_lastname" placeholder="'+txt.user_lname_placeholder+'">';
			html=html+'</div>';
		html=html+'</div>';
		html=html+'<div class="form-group">';
			html=html+'<label for="input_email" class="col-sm-2 control-label">'+txt.user_email+':</label>';
			html=html+'<div class="col-sm-10">';
			/*if(type=='edit'){
				html=html+'<input type="text" class="form-control" disabled="disabled" name="email" value="'+data.email+'" id="input_email" placeholder="'+txt.user_email_placeholder+'">';
			}else{*/
				html=html+'<input type="text" class="form-control" name="email" value="'+data.email+'" id="input_email" placeholder="'+txt.user_email_placeholder+'">';
			//}
			html=html+'</div>';
		html=html+'</div>';
		
	
			
		html=html+'<div class="form-group">';
			html=html+'<label for="input_role" class="col-sm-2 control-label">'+txt.user_role+':</label>';
			html=html+'<div class="col-sm-10">';
				html=html+'<select name="level" id="input_role" class="form-control">';
				html=html+'<option disabled="true" selected="selected">'+txt.user_role_placeholder+'</option>';
				$.each(role, function(k,v){
					html=html+'<option value="'+k+'" ';
					if(data.role==k){
					html=html+'selected="selected" ';
					}
					html=html+'>'+v+'</option>';
				});
				html=html+'</select>';
			html=html+'</div>';
		html=html+'</div>';
	
		
		
 html=html+'</form>';
return html;
}
