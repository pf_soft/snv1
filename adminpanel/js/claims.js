$( document ).ready(function() {
	console.log( "claims ready!" );
	$("#contentAdminLangSetting").on("change", "select#claimAction", function(e) {
		c = confirm('You a sure?');
		if(c){
			event_id 	= $(this).attr('event_id');
			model 		= $(this).attr('model');
			autor_id 	= $(this).attr('autor_id');
			action 	= $(this).val();
		
			if(action!='empty'){
				var dataPost = {
				ajaxAdminpanel: "yep",
				dataType: action, 
				arg: {"event_id":event_id,"model":model,"autor_id":autor_id}
				};
				saveDataClaim(dataPost);
			}
		}
	});
});	
function saveDataClaim(dataPost){
$.ajax({
	  type: "POST",
	  dataType: "json",
	  url: '/?action=adminpanel/claims/',
	  data: dataPost,
	  success: function(ansv){
			
				$("#contentAdminLangSetting tr#container_"+ansv.container).remove();
				$("#messageBlock").show();
				$("#messageBlock").html(txt.txt_saved_success);
				setTimeout(function(){
                      $("#messageBlock").hide();
                    }, 3500);
				
			
		},
	  error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
		console.log( jqXHR  );
		}
	});	
return true;	

}