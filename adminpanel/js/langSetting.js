$( document ).ready(function() {
	console.log( "langSetting ready!" );
	
	getDataLangTable({"str":"","byParam":""});
	$("#contentAdminLangSetting").on("click", "button#addDataLangBtn", function(e) {
		data = $("#contentAdminLangSetting form#addDataLang").serialize();
		var dataPost = {
		ajaxAdminpanel: "yep",
		dataType: "saveDataLang", 
		arg: data
		};
		saveDataLang(dataPost);
	});
	$("#contentAdminLangSetting").on("keyup", "input#filterLangTable", function(e) {
		str = $(this).val();
		byParam = $(this).attr('byParam');
		if(str.length!=1){
		arg = {
			"str":str,
			"byParam":byParam
			};
		getDataLangTable(arg);
		}
	});
});

function saveDataLang(dataPost){
$.ajax({
	  type: "POST",
	  dataType: "json",
	  url: '/?action=adminpanel/lang_settings/',
	  data: dataPost,
	  success: function(ansv){
			if(ansv.message=='yep'){
				html = getDataLangTableHTML(ansv);
				$("#contentAdminLangSetting tbody#dataLangTable").html(html);
				 $("#messageBlock").show();
				$("#messageBlock").html(txt.txt_saved_success);
				setTimeout(function(){
                      $("#messageBlock").hide();
                    }, 3500);
				$("#contentAdminLangSetting form#addDataLang input").val('');
			}
		},
	  error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
		console.log( jqXHR  );
		}
	});	
return true;	

}
function getDataLangTable(arg){
	var dataPost = {
		ajaxAdminpanel: "yep",
		dataType: "getDataLangTable", 
		arg: arg
		};
	$.ajax({
		  type: "POST",
		  dataType: "json",
		  url: '/?action=adminpanel/lang_settings/',
		  data: dataPost,
		  success: function(ansv){
				if(ansv.message=='yep'){
					html = getDataLangTableHTML(ansv);
					$("#contentAdminLangSetting tbody#dataLangTable").html(html);
				}
			},
		  error: function(jqXHR,textStatus){alert( "Request failed: " + textStatus);
			console.log( jqXHR  );
			}
		});	
	return true;	
}
function getDataLangTableHTML(ansv){
	 var html = '';
	$.each(ansv.dataForTable, function(k1, dFT) {
		html=html+'<tr>';
		html=html+'<td>'+k1+'</td>';
		
		$.each(ansv.LangDataPerID, function(k2, LDPID) {
			html=html+'<td>'+dFT[LDPID.code]+'</td>';
		});
		
		html=html+'</tr>';
	});
	return html;
}