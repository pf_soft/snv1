<?
#################################
# 	DKZ v. 2.0 					#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
////// Class init Adminpanel app.  \\\\\\
//
///////////////////////////////////////////////////////////////////

$appUrl = '/?action=adminpanel/';
$currentUrl = '/?action=adminpanel/';
$currentAction = array();
$txt['txt_exit']=$this->lang->get('Logout');
$txt['txt_add']=$this->lang->get('add');
$txt['txt_edit']=$this->lang->get('edit');
$txt['txt_delete']=$this->lang->get('delete');
$txt['txt_save']=$this->lang->get('save');
$txt['txt_close']=$this->lang->get('close');
$txt['txt_saved_success']=$this->lang->get('txt_saved_success');
$txt['no']=$this->lang->get('no');
$txt['yes']=$this->lang->get('yes');
$this->hub->set('txt',$txt);
$Sections['users']=array(	
			'title'		=>$this->lang->get('txt_user_managment'),
			'class'		=>'userManagment',
			'tpl'  		=>'usersManagment.html',
			'current'  	=>0,
			'display'	=>1,
			);



$Sections['lang_settings']=array(	
			'title'		=>$this->lang->get('txt_lang_settings'),
			'class'		=>'langSettings',
			'tpl'  		=>'lang_setting.html',
			'current'  	=>0,
			'display'	=>1,
			);
$Sections['claims']=array(	
			'title'		=>$this->lang->get('txt_claims'),
			'class'		=>'claims',
			'tpl'  		=>'claims.html',
			'current'  	=>0,
			'display'	=>1,
			);

		
if(isset($this->Action[1])){
	$action = $this->Action[1];
	
	if(array_key_exists($action,$Sections)!==false){
		if(isset($Sections[$action]['class'])){
			$currentUrl.=$action.'/';
			if(isset($this->request->post['ajaxAdminpanel'])){
				$func = $this->request->post['dataType'];
				$arg = $this->request->post['arg'];
				$this->load->controller($Sections[$action]['class'].'/'.$func,$arg);
			}else{
				$this->load->controller($Sections[$action]['class']);
			}	
		}
		$Sections[$action]['current']=1;
		$currentAction=$Sections[$action];
	}
}

$this->Smarty->assign("currentPatch",$app['frontend']);
$this->Smarty->assign("currentAction",$currentAction);
$this->Smarty->assign("currentUser",$this->currentUser);
$this->Smarty->assign("appUrl",$appUrl);
$this->Smarty->assign("currentUrl",$currentUrl);
$this->Smarty->assign("txt",$this->hub->get('txt'));
$this->Smarty->assign("txt_json",json_encode($this->hub->get('txt')));
$this->Smarty->assign("Sections",$Sections);
$this->Smarty->assign("languages",$this->lang->languages);

?>