<?
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerLangSettings extends Controller{
	
	public function index($arg=array()) {
		$this->load->model('langSettings');
		$this->hub->set('txt',$this->model_adminpanel_langSettings->getTXT());
		
		$dataForTable = $this->model_adminpanel_langSettings->getLanguagesData();
		
		
		$this->Smarty->assign("dataForTable",$dataForTable);
	}
	public function saveDataLang($arg){
		parse_str(html_entity_decode($arg), $data);
		$data=$data['DataLang'];
		$postData = array();
		$this->table = "FW_LANGUAGES_VAL";
		$this->fields = array(
						'id',
						'lang_val_id',
						'lang_id',
						'lang_id_sense');
		foreach($data as $saveType=>$items){
			foreach($items as $id=>$item){
				if($saveType=='UPDATE'){
					$postData['id']=$id;
				}
				$postData['lang_val_id']=$item['lang_val_id'];
				unset($item['lang_val_id']);
				foreach($item as $k=>$v){
					$postData['lang_id']=$k;
					$postData['lang_id_sense']=$v;
					$this->postData=$postData;
					$this->save($saveType);
				}
			}
		} 
		$this->cache->delete('translateData');
		$this->cache->delete('languagesData');
		$t['str']='';
		$this->getDataLangTable($t);
	}
	public function getDataLangTable($arg) {
		$return = array();
		$this->load->model('langSettings');
		$data = $this->model_adminpanel_langSettings->getLanguagesData();
		if(isset($arg['str'])){
			if(strlen($arg['str'])>0){
				$filteredItems = array();
				$filterData = array();
				foreach ($data['dataForTable'] as $k=>$v){
					if(array_key_exists($arg['byParam'],$v)){
						$filterData[$v['lang_val_id']]=$v[$arg['byParam']];
					}
				}
				$input = preg_quote($arg['str'], '~');
				$result = preg_grep('~' . $input . '~', $filterData);
				foreach ($data['dataForTable'] as $k=>$v){
					if(array_key_exists($v['lang_val_id'],$result)){
						$filteredItems[$v['lang_val_id']]=$v;
					}
				}
				$data['dataForTable'] = $filteredItems;
			}
		}
		$return = $data;
		$return['message'] = 'yep';
		echo json_encode($return);
		exit();
	}
}
?>