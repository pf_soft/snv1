<?php
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
class ControllerClaims extends Controller{
	
	public function index($arg=array()) {
		$this->load->model('claims');
		$this->load->model('user','dkz');
		$models = array(
					'vizitki'	=> array('title'=>$this->lang->get('Media'),'cnt'=>0),
					'product'	=> array('title'=>$this->lang->get('Products'),'cnt'=>0),
					'custom'	=> array('title'=>$this->lang->get('Customs request'),'cnt'=>0),
					'transport'	=> array('title'=>$this->lang->get('Transport request'),'cnt'=>0),
					'forumnews'	=> array('title'=>$this->lang->get('forumnews'),'cnt'=>0),
					'mynews'	=> array('title'=>$this->lang->get('mynews'),'cnt'=>0),
				);
		$user_ids 	= [];
		$event_ids 	= [];
		$events 	= []; 
		$users 		= []; 
		$claims = $this->model_adminpanel_claims->getBy('status=1','updated_at DESC');
		if($claims){
			foreach($claims as $item){
				$user_ids[$item['user_id']]=$item['user_id'];
				$user_ids[$item['autor_id']]=$item['autor_id'];
				if(!isset($event_ids[$item['event_type']][$item['event_id']]['cnt'])){
					$event_ids[$item['event_type']][$item['event_id']]['cnt']=1;
				}else{
					$event_ids[$item['event_type']][$item['event_id']]['cnt']++;
				}
				$models[$item['event_type']]['cnt']++;
				$event_ids[$item['event_type']][$item['event_id']]['claims'][]=$item;
			}
			foreach($event_ids as $event_model=>$ids){
				$model = 'model_dkz_'.$event_model;
				$this->load->model($event_model,'dkz');
				$E_data = $this->$model->getBy('id IN ('.implode(",",array_keys($ids)).')','id DESC');
				foreach($E_data as $item){
					$events[$event_model][$item['id']] = $item;
				}
			}
			$str = 'id IN('.implode(',',$user_ids).')';
			$U_data = $this->model_dkz_user->getPartnersBy($str);
			foreach($U_data as $item){
				$users[$item['id']] = $item;
			}					
		}
		$this->Smarty->assign("dataForTable",$event_ids);
		$this->Smarty->assign("events",$events);
		$this->Smarty->assign("users",$users);
		$this->Smarty->assign("models",$models);
	}
	public function ban($arg){
		$this->setBan($arg);
		$this->changeStatus($arg);
		$container = $arg['event_id'].'_'.$arg['model'];
		echo json_encode(array("container"=>$container));
		exit();
	}
	public function del($arg){
		$this->delPost($arg);
		$this->changeStatus($arg);
		$container = $arg['event_id'].'_'.$arg['model'];
		echo json_encode(array("container"=>$container));
		exit();
	}
	public function ban_del($arg){
		$this->delPost($arg);
		$this->setBan($arg);
		$this->changeStatus($arg);
		$container = $arg['event_id'].'_'.$arg['model'];
		echo json_encode(array("container"=>$container));
		exit();
	}
	public function ignore($arg){
		$this->changeStatus($arg);
		$container = $arg['event_id'].'_'.$arg['model'];
		echo json_encode(array("container"=>$container));
		exit();
	}
	private function delPost($arg){
		$this->postData = array();
		$this->fields = array();
		$this->load->model($arg['model'],'dkz');
		$model = 'model_dkz_'.$arg['model'];
		$this->table = $this->$model->table;
		$fields = $this->$model->getRows();
		$this->fields = $fields;
		$this->postData['id'] = $arg['event_id'];
		$this->destroy();
		$this->load->model('comments','dkz');
		$sql = "DELETE FROM ".$this->model_dkz_comments->table." 
						WHERE 
						 event_id=".$arg['event_id']." 
						AND event_type='".$arg['model']."' 
						";
		$this->db->customSQL($sql);
		$sql = "DELETE FROM user_follower_events 
						WHERE 
						 event_id=".$arg['event_id']." 
						AND event_type='".$arg['model']."' 
						";
		$this->db->customSQL($sql);	
		$sql = "DELETE FROM user_follower_comments 
						WHERE 
						 event_id=".$arg['event_id']." 
						AND event_type='".$arg['model']."' 
						";
		$this->db->customSQL($sql);	
		
		$this->db->customSQL("DELETE FROM  `user_subscriber_events` 
										WHERE event_id=".$arg['event_id']." 
										  and event_type='".$arg['model']."' 
										  ");
		return true;
	}
	private function setBan($arg){
		$sql = "UPDATE klients SET ban=1 WHERE id=".$arg['autor_id']." ";
		$this->db->customSQL($sql);
		return true;
	}
	private function changeStatus($arg){
		$sql = "UPDATE claim_events SET status=0 WHERE event_id=".$arg['event_id']." AND event_type='".$arg['model']."' ";
		$this->db->customSQL($sql);
		return true;
	}
}
?>