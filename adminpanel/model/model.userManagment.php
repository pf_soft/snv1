<?
class ModelUserManagment extends Model {

public function getUsersRole(){
	$usersRole = array(
			2=>"Admin",
			1=>"User",
			99=>"New",
			
			);
	return 	$usersRole;	
}
public function getUserByID($id){
	$sqlData['table'] = "klients";
	$sqlData['column'] = " * ";
	$sqlData['where'] = "id='".$id."' ";
	
	$userData = $this->db->get($sqlData);
	if(count($userData['arr'])>0){
		if($userData['arr'][0]['level']!=1 and $userData['arr'][0]['level']!=2){
				$userData['arr'][0]['level']=99;
			}
	return $userData['arr'][0];
	}else{
		return false;
	}
}

public function getAllUsers($limit=false){
	$return = array();
	$sqlData = array();
	$sqlData['table'] = "klients";
	$sqlData['column'] = " * ";
	$sqlData['where'] = "1 ORDER BY level asc";
	
	if($limit)
		$sqlData['where'].=" LIMIT ".$limit;
	
	$userData = $this->db->get($sqlData);
	if(count($userData['arr'])){
		foreach($userData['arr'] as $item){
			if($item['level']!=1 and $item['level']!=2){
				$item['level']=99;
			}
			$return[$item['id']]=$item;
		}
	}
	return $return;
}


public function getTXT(){
	$txt=$this->txt;
	$txt['add_user']=$this->lang->get('add_user');
	$txt['add_user_title']=$this->lang->get('add_user_title');
	
	$txt['user_fname']=$this->lang->get('user_fname');
	$txt['user_fname_placeholder']=$this->lang->get('user_fname_placeholder');
	$txt['user_lname']=$this->lang->get('user_lname');
	$txt['user_lname_placeholder']=$this->lang->get('user_lname_placeholder');
	$txt['user_email']=$this->lang->get('user_email');
	$txt['user_email_placeholder']=$this->lang->get('user_email_placeholder');
	
	$txt['user_newpassword']=$this->lang->get('user_newpassword');
	$txt['user_password']=$this->lang->get('user_password');
	$txt['user_password_placeholder']=$this->lang->get('user_password_placeholder');
	$txt['user_repeatpass_placeholder']=$this->lang->get('user_repeatpass_placeholder');
	$txt['user_change_password']=$this->lang->get('user_change_password');
	
	$txt['user_role']=$this->lang->get('user_role');
	$txt['user_role_placeholder']=$this->lang->get('user_role_placeholder');
	
	$txt['user_active']=$this->lang->get('user_active');
	$txt['user_sendmail_info']=$this->lang->get('user_sendmail_info');
	return $txt;
}
}
?>