
<?
class ModelLangSettings extends Model {
	public function getLanguagesData(){
		$return = array();
		if(false !==$this->cache->get('languagesData')){
			$return = $this->cache->get('languagesData');
			return $return;
		}
		$dataForTable = array();
		$LangDataPerID = array();
		$LangDataPerCode = array();
		$sqlData = array();
		$sqlData['table'] = "FW_LANGUAGES";
		$sqlData['column'] = " * ";
		$sqlData['where'] = "1=1";
		$LangData = $this->db->get($sqlData);
		$sqlData['table'] = "FW_LANGUAGES_VAL";
		$LangValData = $this->db->get($sqlData);
		
		foreach($LangData['arr'] as $k=>$v){
			$LangDataPerID[$v['id']]=$v;
		}
		foreach($LangData['arr'] as $k=>$v){
			$LangDataPerCode[$v['code']]=$v;
		}
		
		foreach($LangValData['arr'] as $k=>$v){
			$dataForTable[$v['lang_val_id']]['lang_val_id']=$v['lang_val_id'];
			$dataForTable[$v['lang_val_id']][$LangDataPerID[$v['lang_id']]['code']]=$v['lang_id_sense'];
		}
		
		$return['dataForTable']=$dataForTable;
		$return['LangDataPerID']=$LangDataPerID;
		$return['LangDataPerCode']=$LangDataPerCode;
		$this->cache->set('languagesData',$return);
		return $return;
	}
	public function getTXT(){
		$txt=$this->txt;
		$txt['txt_search_by_lang_val_id']=$this->lang->get('txt_search_by_lang_val_id');
		$txt['txt_search_by']=$this->lang->get('txt_search_by');
		$txt['txt_lang_val_id']=$this->lang->get('txt_lang_val_id');
		return $txt;
	}
}
?>
