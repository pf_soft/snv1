<?php
#################################
# 	DKZ v. 2.0		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
//////////////////////////Init   file\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//
// Get and defined DIRNAME->ROOT_DIR ->CORE_DIR+
// Init CORE INTERFACE /core/init.php+
// Include functions for core testing+
// TESTING CORE COMPONENTS+
//echo 'ok';
//exit();
error_reporting(E_ALL); ini_set('display_errors', 1);
date_default_timezone_set('Europe/Kiev');
$load_time_start = microtime(1);
define ( 'ROOT_DIR', __DIR__ );

define ( 'CORE_DIR', ROOT_DIR . '/core' );
define ( 'DATA_DIR', ROOT_DIR . '/core/data' );
define ( 'CACHE_DIR', ROOT_DIR . '/core/temp/cache/' );
define ( 'ADMIN_PATCH', 'adminpanel' );
define ( 'ADMIN_MAIN_DIR', ROOT_DIR.'/adminpanel' );
define ( 'DKZ_PATCH', 'dkz' );
define ( 'DKZ_MAIN_DIR', ROOT_DIR.'/app/dkz' );
require_once ROOT_DIR . '/core/init.php';
$load_time = microtime(1) - $load_time_start;

///echo '<br> Load time: ' . $load_time;