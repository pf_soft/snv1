<?php
#################################
# DKZ v. 2.0__2019	 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################

//////////////////////////CORE INTERFACE file\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//
// Init DB CONNECT+
// Init core+
//
//
//

require_once( CORE_DIR . '/data/phpsql.php');

require_once(CORE_DIR.'/libs/class.coreLog.php');

require_once(CORE_DIR.'/libs/class.Cache.php');
require_once(CORE_DIR.'/libs/cache/file.php');

require_once(CORE_DIR.'/libs/class.Lang.php');

require_once(CORE_DIR.'/libs/class.Request.php');
require_once(CORE_DIR.'/libs/class.Response.php');
require_once(CORE_DIR.'/libs/class.Session.php');






require_once( CORE_DIR . '/components/class.Hub.php');
require_once( CORE_DIR . '/components/class.Controller.php');
require_once( CORE_DIR . '/components/class.View.php');
require_once( CORE_DIR . '/components/class.Model.php');
require_once( CORE_DIR . '/components/class.Loader.php');
require_once( CORE_DIR . '/core.php');

$db = new phpsql;

$hub = new Hub;
$hub->set('db',$db);


$load = new Loader($hub);
$hub->set('load',$load);

$cache = new Cache('file');
$hub->set('cache',$cache);

$Session = new Session;
$hub->set('session',$Session);

$Request = new Request;
$hub->set('request',$Request);

$Response = new Response;
$hub->set('response',$Response);

$Lang = new Lang($hub);
$hub->set('lang',$Lang);

$core = new core($hub);
$core->initCoreAndStartFW();
/*$test = new test($hub);
$test->index();
echo '<pre>';

$data = array(
			'table'=>'users',
			'column'=>' * ',
			'where' => '1=1'
			);
print_r($db->showTables());
print_r($db->get($data));*/

?>