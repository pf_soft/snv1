<?php
#################################
# 	DKZ v. 2.0__2019 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################

Class core
{
    public $hub;
    public $Action = array();
    public $currentUser = false;
    public $currentView = false;
    private $Smarty = false; //or object
    private $Apps = array();

    function __construct($hub)
    {
        $this->hub = $hub;
    }

    public function __get($key)
    {
        return $this->hub->get($key);
    }

    public function __set($key, $value)
    {
        $this->hub->set($key, $value);
    }

    public function initCoreAndStartFW()
    {
        $this->initErrorLog();

        $this->setThisApps();
        $this->initCoreRouting();
        $this->initSmarty();
        // Init external libraries END<---
        $this->accessControl();
        $this->onlineControl();
        $this->onlineSet();
        $this->countriesSet();
        $this->initCurrentView();
        //Start Framework END<---
    }

	private function countriesSet()
	{
		//if(false ===$this->cache->get('countries')){
			$lang = $this->lang->getLang();
			$sqlData = array(
                "table" => "countries",
                "column" => "country_id,name",
                "where" => "status>0 and language='".$lang."'",
            );
			$data = $this->db->get($sqlData);
			$countries = array();
			if(count($data['arr'])){
				foreach($data['arr'] as $item){
					$countries[$item['country_id']]=$item['name'];
				}
			}
			$this->cache->set('countries',$countries);	
		//}
		return true;
	}
	private function onlineSet()
	{
		$time = time()-(10*60);
		$sqlData = array(
                "table" => "online",
                "column" => "user_id",
                "where" => "lastenter>'".$time."'",
            );
		$data = $this->db->get($sqlData);
		$uids = array();
		if(count($data['arr'])){
			foreach($data['arr'] as $item){
				$uids[$item['user_id']]=$item['user_id'];
			}
		}
		$this->cache->set('whoOnline',$uids);	
	}
    private function onlineControl()
	{
		if(!isset($this->currentUser['id'])){
			return false;
		}
		if(!isset($this->currentUser['level'])){
			return false;
		}
		if($this->currentUser['level']>3){
			return false;
		}
		$ip = $_SERVER['REMOTE_ADDR'];
		$uid = $this->currentUser['id'];
		$sqlData = array(
                "table" => "online",
                "column" => "id",
                "where" => "user_id='".$uid."'",
            );
		$data = $this->db->get($sqlData);
		$lastenter = time();
		if(isset($data['arr'][0])){
			$sql = "UPDATE online SET lastenter='".$lastenter."', ip='".$ip."', level='".$this->currentUser['level']."' WHERE user_id='".$uid."'";
		}else{
			$sql = "INSERT INTO `online` ( `user_id`, `lastenter`, `ip`, `level`)  VALUES (".$uid.", ".$lastenter.", '".$ip."', ".$this->currentUser['level'].")";
		}
		$this->db->customSQL($sql);
	}
    private function accessControl()
    {// check access rules
        try {

            $this->load->corelib('coreAccessManager');
            $this->currentUser = $this->lib_coreAccessManager->declareThisCurrentUser();
            $this->hub->set('currentUser', $this->currentUser);
            return true;
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }

    private function initCoreRouting()
    {// installer route system
        try {
            $this->load->corelib('coreRouting');
            $data = $this->lib_coreRouting->declareThisView();
            $this->Action = $this->lib_coreRouting->declareThisActions();
            $this->hub->set('Action', $this->Action);
            define('TPL_DIR', $data["frontend"] . 'frontend');
            define('COMP_DIR', $data["frontend"] . 'frontend_c');
            $this->setThisCurrentView($data);
            return true;
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }

    private function initCurrentView()
    {
        try {
            if (!$this->currentView) {
                $message = "api::initCurrentView #0->Collapse method. thisCurrentView IS FALSE";
                throw new coreLog($message, 0, "red");
            }
            $app = $this->currentView;

            $viewsFile = $app["location"] . $app["loader"];
            $dysplayTPL = "index.html";

            if (file_exists($viewsFile) === false) {
                $message = "api::initCurrentView #0->file " . $viewsFile . " not found.";
                throw new coreLog($message, 0, "red");
            }

            require_once $viewsFile;

            //$this->Smarty->
            $this->Smarty->display($dysplayTPL);
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }

    private function setThisCurrentView($data = false)
    {// set data on the current view
        try {
            if (!$data) {
                $message = "api::setThisCurrentView #0->Collapse method. Current View is false.";
                throw new coreLog($message, 0, "red");
            }
            if (!isset($data["location"])) {
                $message = "api::setThisCurrentView #0->Collapse method. Current View location not isset.";
                throw new coreLog($message, 0, "red");
            }
            if (!isset($data["loader"])) {
                $message = "api::setThisCurrentView #0->Collapse method. Current View loader not isset.";
                throw new coreLog($message, 0, "red");
            }
            $this->currentView = $data;
            $this->hub->set('currentView', $this->currentView);
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }

    private function setThisApps()
    {// GET data from Table FW_APPS
        try {
            $this->Apps = array(
                "FW" => array(
                    "name" => "FW",
                    "location" => ROOT_DIR . "/core/",
                    "loader" => "init.php",
                    "current" => 0,
                ),
                ADMIN_PATCH => array(
                    "name" => "adminpanel",
                    "location" => ADMIN_MAIN_DIR . '/',
                    "loader" => "admin.php",
                    "current" => 0,
                    "frontend" => ADMIN_PATCH . '/',
                ),
				DKZ_PATCH => array(
                    "name" => "dkz",
                    "location" => DKZ_MAIN_DIR . '/',
                    "loader" => "index.php",
                    "current" => 1,
                    "frontend" => 'app/'.DKZ_PATCH . '/',
                ),
				
            );

            $return = false;
            $sqlData = array(
                "table" => "FW_APPS",
                "column" => "name,location,loader,current",
                "where" => "enable=1",
            );

            $data = $this->db->get($sqlData);

            foreach ($data["arr"] as $app) {
                if (!isset($app["name"])) {
                    $message = "api::setThisApiApps #0->Collapse method. App name not isset";
                    throw new coreLog($message, 0, "red");
                }
                $this->Apps[$app["name"]]["name"] = $app["name"];

                if (!isset($app["location"])) {
                    $message = "api::setThisApiApps #0->Collapse method. App location not isset";
                    throw new coreLog($message, 0, "red");
                }
                $this->Apps[$app["name"]]["location"] = $app["location"];

                if (!isset($app["loader"])) {
                    $message = "api::setThisApiApps #0->Collapse method. App loader not isset";
                    throw new coreLog($message, 0, "red");
                }
                $this->Apps[$app["name"]]["loader"] = $app["loader"];

                if (!isset($app["current"])) {
                    $app["current"] = 0;
                }
                $this->Apps[$app["name"]]["current"] = $app["current"];

                $this->Apps[$app["name"]]["frontend"] = "app/" . $app["name"] . "/";
            }
            /* echo "<pre>";
              print_r($this->Apps);
              exit(); */
            unset($data);
            unset($sqlData);
            $this->hub->set('Apps', $this->Apps);
            return true;
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }

    private function initSmarty()
    {
        require ROOT_DIR . '/libs/Smarty.class.php';
        $this->Smarty = new Smarty;
		$this->Smarty->register_object('lang',$this->lang,null,false);
        $this->hub->set('Smarty', $this->Smarty);
    }

    private function initErrorLog()
    {
        set_error_handler(create_function('$c, $m, $f, $l',
                'if ($c === E_NOTICE) {throw new coreLog($m,1,"yellow");} else {throw new coreLog($m,0,"red");}'), E_ALL ^ E_DEPRECATED);
    }
}
