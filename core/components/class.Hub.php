<?php
#################################
# DKZ v. 2.0					#
#                               #
#################################

/* Class Hub   
registration, storage and transfer of custom data resource types
///// DON'T EDIT THIS \\\\\\
*/
final class Hub{
	private $data = array();
	public function get($key) {
		return (isset($this->data[$key]) ? $this->data[$key] : null);
	}

	public function set($key, $value) {
		$this->data[$key] = $value;
	}

	public function has($key) {
		return isset($this->data[$key]);
	}
}