<?php
#################################
# DKZ v. 2.0					#
#                               #
#################################

/*
  abstract Class View  AnnITFramework
  ///// DON'T EDIT THIS \\\\\\
*/

final class View
{
    private $file;
    private $class;
    private $method;
    private $args = array();

    public function __construct($app, $route, $args = array())
    {
        $path = '';

        // Break apart the route
        $parts = explode('/', str_replace('../', '', (string) $route));


        foreach ($parts as $part) {
            $path .= $part;

            if (is_dir($app . $path)) {
                $path .= '/';

                array_shift($parts);

                continue;
            }

            $file = $app . str_replace(array('../', '..\\', '..'), '', $path) . '.php';

            if (is_file($file)) {
                $this->file = $file;

                $this->class = 'Controller' . preg_replace('/[^a-zA-Z0-9]/', '', $path);

                array_shift($parts);

                break;
            }
        }

        if ($args) {
            $this->args = $args;
        }

        $method = array_shift($parts);

        if ($method) {
            $this->method = $method;
        } else {
            $this->method = 'index';
        }
    }

    public function execute($hub)
    {
        // Stop any magical methods being called
        if (substr($this->method, 0, 2) == '__') {
            return false;
        }

        if (is_file($this->file)) {
            include_once($this->file);

            $class = $this->class;

            $controller = new $class($hub);

            if (is_callable(array($controller, $this->method))) {
                return call_user_func(array($controller, $this->method), $this->args);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
