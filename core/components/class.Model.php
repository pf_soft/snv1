<?
#################################
# DKZ v. 2.0					#
#                               #
#################################

/* 
abstract Class Model  DKZ 

///// DON'T EDIT THIS \\\\\\
*/
abstract class Model {
	public $table;
	private $hub;

	public function __construct($hub) {
		$this->hub = $hub;
	}
	public function __get($key) {
		return $this->hub->get($key);
	}

	public function __set($key, $value) {
		$this->hub->set($key, $value);
	}
	public function getRows()
	{
		return $this->db->showColumns($this->table);
	}
	public function getAll($order,$limit=30,$offset=false){
		$return = false;
		$sqlData['table'] = $this->table;
		$sqlData['column'] = " * ";
		$sqlData['where'] =  ' 1 ';
		$sqlData['where'].=  ' ORDER BY '.$order;
		if($limit)
			$sqlData['where'].=  ' LIMIT '.$limit;
		if($offset)
			$sqlData['where'].=  ' OFFSET '.$offset;
		$Data = $this->db->get($sqlData);
		if(count($Data['arr'])>0)
			$return=$Data['arr'];
			
		return $return;
	}
	public function getBy($str,$order,$limit=30,$offset=false){
		$return = false;
		$sqlData['table'] = $this->table;
		$sqlData['column'] = " * ";
		$sqlData['where'] =  $str;
		$sqlData['where'].=  ' ORDER BY '.$order;
		if($limit)
			$sqlData['where'].=  ' LIMIT '.$limit;
		if($offset)
			$sqlData['where'].=  ' OFFSET '.$offset;
		
		$userData = $this->db->get($sqlData);
		if(count($userData['arr'])>0)
			$return=$userData['arr'];
			
		return $return;
	}
	public function find($id){
		$return = false;
		$sqlData['table'] = $this->table;
		$sqlData['column'] = " * ";
		$sqlData['where'] =  'id='.$id;
		$Data = $this->db->get($sqlData);
		if(isset($Data['arr'][0]))
			$return=$Data['arr'][0];
			
		return $return;
	}
}
?>