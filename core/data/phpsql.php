<?php
#################################
# AnnITFramework v. 2.0_12_2015 #
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
////// Class  pdo lib  AnnITFramework \\\\\\

final class phpsql
{
/**
     * Parsed configuration file
     * @var str[]
     */
    var $config;

    /**
     * Database connection
     * @var resource
     */
    var $db;
    var $lastInsertId;

    function __construct($iniFile = DATA_DIR . '/phpsql.ini')
    {
        $this->config = parse_ini_file($iniFile, TRUE);
        $type = $this->config['database']['type'];
        $db = $this->config['database']['database'];
        $host = $this->config['database']['server'];
        $charset = $this->config['database']['charset'];


        $user = $this->config['database']['username'];
        $pass = $this->config['database']['password'];
        $dsn = "$type:host=$host;dbname=$db";
        $opt = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        );
        if (!$user or ! $pass) {
            $this->unauthorized();
            exit;
        } else {
            try {
                $this->db = new PDO($dsn, $user, $pass, $opt);
                $this->db->query("SET NAMES 'UTF8'");
            } catch (\Exception $ex) {
                echo 'PDOException: ' . $ex->getMessage() . ' dsn: ' . $dsn;
                exit();
            }
        }
    }

    public function customSQL($sql)
    {
        $query = $this->db->exec($sql);
        return true;
    }
	public function showColumns($table)
    {
        $sql = sprintf('SELECT column_name FROM information_schema.columns WHERE table_name =\'%s\'', $table);
        $query = $this->db->query($sql);
        return $query->fetchAll(PDO::FETCH_COLUMN);
    }

    public function showTables()
    {
        $sql = 'SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\'';
        $query = $this->db->query($sql);
        return $query->fetchAll(PDO::FETCH_COLUMN);
    }

    public function get($data,$t=true)
    {
        $return = array();
        $return['form'] = $this->showColumns($data['table']);
        $sql = '';
        $sql.= "SELECT ";
        $sql.=$data['column'];
        $sql.= " FROM ";
		if($t){
        $sql.='`' . $data['table'] . '`';
		}else{
			$sql.=$data['table'];
		}
        $sql.= " WHERE ";
        $sql.=$data['where'];
        try {
			$query = $this->db->query($sql);
            $return['arr'] = $query->fetchAll();
        } catch (PDOException $Exception) {
            echo 'PDOEcxeption: ' . $Exception->getMessage() . "<br/>" . $Exception->getCode();
            exit();
        }
        return $return;
    }

    public function save($data)
    {
        $sql = '';
        if ($data['type'] == 'INSERT') {

            $sql.="INSERT INTO ";
            $sql.='`' . $data['table'] . '`';
            $sql.=" (";
            $sql.=$data['fieldsString'];
            $sql.=" )";
            $sql.=" VALUES ";
            $sql.=" (";
            $sql.=$data['bindParamNamesString'];
            $sql.=" )";
        //    if (isset($data['returning']))
        //        $sql.=" RETURNING " . $data['returning'];
        }
        if ($data['type'] == 'UPDATE') {
            $sql.="UPDATE ";
            $sql.='`' . $data['table'] . '`';
            $sql.=" SET ";
            $sql.=$data['setString'];
            $sql.=" WHERE ";
            $sql.=$data['where'];
        }
        //echo $sql.'<br>'; //exit();
        try {
            $stmt = $this->db->prepare($sql);
            foreach ($data['bindParamNames'] as $param) {
                $key = str_replace(':', '', $param);
                $stmt->bindParam($param, $data['post'][$key]);
            }

            $stmt->execute();
        } catch (PDOException $Exception) {
            echo $Exception->getMessage() . "<br>";
            echo $Exception->getCode();
            exit();
        }
    //    if (isset($data['returning'])) {
    //        $result = $stmt->fetch(PDO::FETCH_ASSOC);
    //       $this->lastInsertId = $result[$data['returning']];
    //    }
    }

    public function tableCleaner($table)
    {
        $this->db->exec("DELETE FROM ` . $table . ` WHERE 1=1");
    }

    public function delete($data)
    {
        $sql = '';
        $sql.="DELETE FROM ";
        $sql.='`' . $data['table'] . '`';
        $sql.=" WHERE ";
        $sql.=$data['where'];
        $stmt = $this->db->prepare($sql);
        foreach ($data['bindParamNames'] as $param) {
            $key = str_replace(':', '', $param);
            $stmt->bindParam($param, $data['post'][$key]);
        }
        $stmt->execute();
    }

    public function getLastID()
    {
        return $this->db->lastInsertId();
    }

    /**
     * Send a HTTP 401 response header.
     */
    function unauthorized($realm = 'PHPSQL')
    {
        header('WWW-Authenticate: Basic realm="' . $realm . '"');
        header('HTTP/1.0 401 Unauthorized');
        exit();
    }
}
