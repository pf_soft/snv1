<?
#################################
# AnnITFramework v. 1.0_02_2015 #
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################

////// Class manages logs from AnnITFramework and app\\\\\\
//
//			/////!!! DON'T EDIT THIS CLASS !!!\\\\\\
///////////////////////////////////////////////////////////////////
define("LOG_FILE_DIR",CORE_DIR.'/logs');
define('LOG_FILE_MAXSIZE','100');
define("LOG_ROTATE",false);
Class coreLog extends Exception{
	
	public $errorType = false;
	public $display = false;
	private $errorNumber = false;
	private $logFilesByErrorType = array(
									'red'=>'/log_red.txt',
									'yellow'=>'/log_yellow.txt',
									'white'=>'/log_err_log.txt',
									);
									
	function __construct($message=false,$errorCode=false,$errorType = "white"){
		$this->errorType = "white";
		if(array_key_exists($errorType,$this->logFilesByErrorType)===true){
			$this->errorType = $errorType;
		}
		$this->display = ($this->errorType!="white")?true:false;
		if($message){
			parent::__construct($message,$errorCode);
			$errorText=$this->registerLog();
			
		}
	}
	public function replyLog($errorText,$errorCode){
		if($errorCode==0){
				die($this->replyMessage($errorText,$errorCode));
			}elseif($errorCode==1){
				$_SESSION["errorText"]=$this->replyMessage($errorText,$errorCode);
				die($this->replyMessage('This site is temporarily unavailable.'.$_SESSION["errorText"],$errorCode));
			}elseif($errorCode==404){
				$_SESSION["errorText"]=$this->replyMessage($errorText,$errorCode);
				header('Location: /?action=oops/404/');
			}elseif($errorCode==999 or !isset($errorCode)){
				$_SESSION["errorText"]=$this->replyMessage($errorText,$errorCode);
				return true;
			}
	}
	private function replyMessage($errorText,$errorCode=false) {
		$return = "<div style='padding:15px; background:pink;  font: 14px \"Courier\"'>";
		$return.= $errorText;
		$return.= "</div>";
		return $return;
	}	
	private function registerLog() {
		$return 	= false;
		$timestamp 	= time();
		$logfile 	= LOG_FILE_DIR.$this->logFilesByErrorType[$this->errorType];
		
		
		$errorID 	= time().'_'.rand(1,99999);
		$message	= $this->getMessage();
		$line		= $this->getLine();
		$file		= $this->getFile();
		$trace 		= ($this->errorType!="white")?$this->getTrace():false;
		$tracetext  = "";
		if($trace){
			foreach($trace as $k=>$v){
				$tracetext.= " #".$k."-> in file:".$v["file"]." at line:".$v["line"];
			}	
		}
		$errArr[$errorID]["id"]			= $errorID;
		$errArr[$errorID]["time"] 		= $timestamp;
		$errArr[$errorID]["line"] 		= $line;
		$errArr[$errorID]["file"] 		= $file;
		$errArr[$errorID]["message"] 	= $message;
		$errArr[$errorID]["trace"] 		= $tracetext;
		
		$errSerialize 	= serialize($errArr)."\n";
		// check for the maximum size
		if (is_file($logfile) AND filesize($logfile)>=(LOG_FILE_MAXSIZE*1024)){
			/*
				check the settings if set LOG_ROTATE,
				then shift the old files with one down and create an empty log 
				if not - Clean and write over the old log
			*/
			if (LOG_ROTATE===true){
				$i=1;
				//assume the old logs in the directory
				while (is_file($logfile.'.'.$i)) { $i++; }
				$i--;
				//each of them in turn increase the number by 1
				while ($i>0) {
				   rename($logfile.'..'.$i,$logfile. '.' .(1+$i--));
				}
				rename ($logfile,$logfile.'.1');
				touch($logfile);
			}elseif(is_file($logfile)) {
				//if we write logs on top, then remove
				//and create a new empty file
				unlink($logfile);
				touch($logfile);
			}
		} 
		/*
		     check whether there is such a file
		     - if not, whether we can create it
		     if there is - if we can write to it
	    */ 
		if(!is_file($logfile)) {
			if (!touch($logfile)) {
				$return='can\'t create log file';
			}
		}elseif(!is_writable($logfile)) {
			$return = 'can\'t write to log file';
		}
		if(error_log($errSerialize, 3, $logfile)){
			
			$return="<span style='background:".$this->errorType.";'>".$errArr[$errorID]["message"].' ID-'.$errorID.'</span> <br/>';
		}
		 return $return;
	}
	public function readLog($type="white"){
		$logfile	= LOG_FILE_DIR.$this->logFilesByErrorType[$type];
		$return		= array();
		//echo $logfile."<br>";
		if (is_file($logfile) and is_writable($logfile)){
			//echo "isset file<br>";
			$logContent = file($logfile);
			//echo "<pre>";
			//print_r($logContent);
			foreach ($logContent as $nmbrStr => $arr){
				//print_r($arr); 
				$data = unserialize($arr);	
				//print_r($data);
				foreach($data as $k=>$v){
					$key  = $v["id"]."_".$nmbrStr;	
				}
				$return[$key] = $data;
			}
		}/*else{
			echo "no file<br>";
		}*/
		return array_reverse($return, true);	
	}
}
?>