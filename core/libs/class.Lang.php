<?php
final class Lang{
 public $languages=array();
 protected $hub;
 protected $defaultLang;
 protected $translateData = array();
	
	function __construct($hub){
		$this->hub=$hub;
		$this->setTranslateData();
	}
	public function __get($key) {
		return $this->hub->get($key);
	}

	public function __set($key, $value) {
		$this->hub->set($key, $value);
	}
	public function getLang(){
		$lang = false;
		if(false !==$this->cache->get('translateData')){
			$data = $this->cache->get('translateData');
			$lang = $data['defaultLang'];
		}else{
			$this->setTranslateData();
			$lang = $this->defaultLang;
		}
		if(isset($this->currentUser['language'])) $lang = $this->currentUser['language'];
		if(isset($this->request->get['lang'])) $lang = $this->request->get['lang'];
		return $lang;
	}
	public function get($key,$lang=false){
		if($lang===false){
			$lang = $this->getLang();
		}
		if(isset($this->translateData[$lang][$key])){
			return $this->translateData[$lang][$key];
		}else{
			return $key;
		}
	
	}
	/*public function languages(){
		if(isset($this->translateData['languages'])){
			return $this->translateData['languages'];
		}else{
			return array();
		}
	
	}*/
	private function setTranslateData(){
		if(false !==$this->cache->get('translateData')){
			$data = $this->cache->get('translateData');
			$this->defaultLang=$data['defaultLang'];
			$this->languages=$data['languages'];
			unset($data['defaultLang']);
			unset($data['languages']);
			$this->translateData=$data;
			return true;
		}
		$sqlData['table'] = "FW_LANGUAGES";
		$sqlData['column'] = " * ";
		$sqlData['where'] = "1";
		$data = $this->db->get($sqlData);
		foreach($data['arr'] as $k=>$v){
			$this->languages[]=$v;
			$sqlData['table'] = "FW_LANGUAGES_VAL";
			$sqlData['column'] = " * ";
			$sqlData['where'] = "lang_id=".$v['id'];
			$values = $this->db->get($sqlData);
			foreach($values['arr'] as $value){
				$this->translateData[$v['code']][$value['lang_val_id']]=$value['lang_id_sense'];
			}
			if($v['default']==1)
				$this->defaultLang=$v['code'];
		}
		$cacheData=$this->translateData;
		$cacheData['defaultLang']=$this->defaultLang;
		$cacheData['languages']=$this->languages;
		
		
		$this->cache->set('translateData',$cacheData);
		return true;
	}

}