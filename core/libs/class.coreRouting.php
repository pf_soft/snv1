<?php
#################################
# AnnITFramework v. 1.0_02_2015 #
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
////// Class manages links from AnnITFramework and app\\\\\\
//
//			/////!!! DON'T EDIT THIS CLASS !!!\\\\\\
///////////////////////////////////////////////////////////////////

Class coreRouting
{

    protected $hub;

    function __construct($hub)
    {
        $this->hub = $hub;
        $this->setThisView();
    }

    public function __get($key)
    {
        return $this->hub->get($key);
    }

    public function __set($key, $value)
    {
        $this->hub->set($key, $value);
    }

    public function declareThisView()
    {
        return $this->view;
    }

    public function declareThisActions()
    {
        return $this->Actions;
    }

    private function setThisView()
    {
        try {
            $actionDefault = false;
            if (!$this->Apps) {
                $message = "coreRouting::setThisView #0->App not found.";
                throw new coreLog($message, 0, "red");
            }
            $apps = $this->Apps;
            foreach ($apps as $k => $v) {
                if ($v["current"] == 1) {
                    $actionDefault = $apps[$k];
                }
            }
            if (!$actionDefault) {
                $message = "coreRouting::setThisView #0->Action 'Default' not isset.";
                throw new coreLog($message, 0, "red");
            }
            if (!empty($this->request->get["action"])) {
                $explodeAction = explode("/", $this->request->get["action"]);
                $action = array();
                foreach ($explodeAction as $k => $v) {
                    if (!empty($v))
                        $action[] = trim($v);
                }
                if ($action) {

                    if ($action[0] == 'exit') {
                        unset($_SESSION['currentUser']);
                        unset($_SESSION['operator_extra']);
                        header('Location: /index.php');
                        return true;
                    }

                    if (array_key_exists($action[0], $apps) === false) {
                        throw new coreLog("Key '" . $action[0] . "' doesn't exist", 404);
                    }

                    $this->Actions = $action;
                    $app = $apps[$action[0]];
                    $this->view = $app;
                } else {
                    $this->view = $actionDefault;
                    $this->Actions = array(0 => $actionDefault['name'],);
                }
            } else {
                $this->view = $actionDefault;
                $this->Actions = array(0 => $actionDefault['name'],);
            }

            if (isset($this->request->get["printlog"])) {
                $coreLog = new coreLog();
                $log0 = $coreLog->readLog();
                $log1 = $coreLog->readLog("yellow");
                $log2 = $coreLog->readLog("red");
                echo "<h1>Framework logs</h1><pre>";
                echo "<h2>red</h2>";
                print_r($log2);
                echo "<h2>yellow</h2>";
                print_r($log1);
                echo "<h2>default</h2>";
                print_r($log0);
                echo "______________________________";
                exit();
            }
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }
}
