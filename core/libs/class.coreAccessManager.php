<?php
#################################
# 	DKZ v. 2.0 		 			#
#                               #
# Created By "THE CLUB"         #
# http://pro-club.biz           #
#################################
////// Class Access Control from AnnITFramework and app\\\\\\
//
//			/////!!! DON'T EDIT THIS CLASS !!!\\\\\\
///////////////////////////////////////////////////////////////////

Class coreAccessManager
{
    protected $hub;
    protected $currentUser = false;
    protected $needAuthentication = false;

    function __construct($hub)
    {
        $this->hub = $hub;
        $acton = "/";
        if (count($this->Actions) > 0)
            $acton = implode("/", $this->Actions);
        if (isset($_SESSION['userTravel'])) {
            $requestedCtn = sizeof($_SESSION['userTravel']);
            if ($_SESSION['userTravel'][$requestedCtn - 1]['acton'] != $acton)
                $_SESSION['userTravel'][] = array('time' => time(), 'acton' => $acton,);
        } else {
            $_SESSION['userTravel'] = array();
            $_SESSION['userTravel'][] = array('time' => time(), 'acton' => $acton,);
        }
        $this->identifiesUser();
        $this->controlAccess();
        if ($this->currentUser === False)
            throw new coreLog("Needs authentication", 999);
        //throw new coreLog("Access is false",0,"red");
        return true;
    }

    public function __get($key)
    {
        return $this->hub->get($key);
    }

    public function __set($key, $value)
    {
        $this->hub->set($key, $value);
    }

    public function declareThisCurrentUser()
    {
        return $this->currentUser;
    }

    private function controlAccess()
    {
        try {

            if ($this->needAuthentication === true) {
                header($this->request->server['SERVER_PROTOCOL'] . ' 401 Unautorized');
                $this->response->redirect('/?action=dkz/singin/', 303);
                //header('Location: /?action=oops/401/');
                //echo '1';
            } else {
                $this->setUserData();
                $this->Access = $this->currentUser['ruleCurrentPage']['read'];
                /* echo '<pre>';
                  print_r($this->currentUser);
                  exit(); */
                if ($this->Access === False) {
                    //header($this->request->server['SERVER_PROTOCOL'] . ' 403 Forbidden');
                    $this->response->redirect('/?action=dkz/singin/', 303);
                    //header('Location: /?action=oops/403/');
                    //return false;
                    //echo '2';
                }
            }
            //print_r($this->currentUser);
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }

    private function identifiesUser()
    {
        try {
            //unset($_SESSION['currentUser']);
            if (isset($_SESSION['currentUser'])) {

                $this->currentUser = $_SESSION['currentUser'];
            } else {
                $this->setGuestData();
            }
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }

    private function setUserData()
    {
        try {
            $role = $this->getUserRole();
            $RulesForRole = $this->getRulesForRole();
            $currentRules = array();
            $lastKey = count($this->Actions) - 1;

            for ($i = $lastKey; $i >= 0; $i--) {
                $item = $this->Actions[$i];
                $GID = $this->currentUser['groupID'];
                if (
                    array_key_exists($item, $RulesForRole) !== false
                    and
                    array_key_exists($GID, $RulesForRole[$item]) !== false
                ) {
                    $this->currentUser['ruleCurrentPage'] = $RulesForRole[$item][$GID];

                    return true;
                }
            }

            $this->currentUser['ruleCurrentPage'] = array(
                'read' => true,
                'edit' => true,
                'delete' => true,
                'implementation' => false,
            );
            return true;
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }

    private function setGuestData()
    {
        try {
            $role = $this->getUserRole();
            $RulesForRole = $this->getRulesForRole();
            $currentRules = array();


            $this->currentUser = array();
            $this->currentUser['name'] = 'Guest';
            $this->currentUser['groupID'] = 99;
            $lastKey = count($this->Actions) - 1;

            for ($i = $lastKey; $i >= 0; $i--) {
                $item = $this->Actions[$i];
                $GID = $this->currentUser['groupID'];
                if (
                    array_key_exists($item, $RulesForRole) !== false
                    and
                    array_key_exists($GID, $RulesForRole[$item]) !== false
                ) {
                    if ($RulesForRole[$item][$GID]['read'] === false)
                        $this->needAuthentication = true;
                    return true;
                }
            }
            return true;
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }

    private function getUserRole()
    {
        try {
            return array(
                0 => 'Developer',
                1 => 'User',
                2 => 'Admin',
				99 => 'Guest',
            );
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }

    private function getRulesForRole()
    {
        try {
            $rules = array();
            $rules['full_access'] = array(
                'read' => true,
                'edit' => true,
                'delete' => true,
                'implementation' => true,
            );
            $rules['expanded_access'] = array(
                'read' => true,
                'edit' => true,
                'delete' => true,
                'implementation' => false,
            );
            $rules['standard_access'] = array(
                'read' => true,
                'edit' => true,
                'delete' => false,
                'implementation' => false,
            );
            $rules['read_access'] = array(
                'read' => true,
                'edit' => false,
                'delete' => false,
                'implementation' => false,
            );
            $rules['read_access'] = array(
                'read' => true,
                'edit' => false,
                'delete' => false,
                'implementation' => false,
            );
            $rules['no_access'] = array(
                'read' => false,
                'edit' => false,
                'delete' => false,
                'implementation' => false,
            );
            $RulesForRole = array();
            //access to the applications
            // example $RulesForRole[%action%][%role_id%]=$rules['full_access'];
            //stat agregator
           

            //app adminpanel
            $RulesForRole[ADMIN_PATCH][0] = $rules['full_access'];
            $RulesForRole[ADMIN_PATCH][2] = $rules['expanded_access'];
            $RulesForRole[ADMIN_PATCH][1] = $rules['no_access'];
            $RulesForRole[ADMIN_PATCH][99] = $rules['no_access'];
            //app dkz
            $RulesForRole['dkz'][0] = $rules['full_access'];
            $RulesForRole['dkz'][1] = $rules['expanded_access'];
            $RulesForRole['dkz'][2] = $rules['expanded_access'];
            $RulesForRole['dkz'][99] = $rules['expanded_access'];
            //app dkz. Part mainpage
            $RulesForRole['mainpage'][0] = $rules['full_access'];
            $RulesForRole['mainpage'][1] = $rules['expanded_access'];
            $RulesForRole['mainpage'][2] = $rules['expanded_access'];
            $RulesForRole['mainpage'][99] = $rules['no_access'];
			//app dkz. Part forum
            $RulesForRole['forum'][0] = $rules['full_access'];
            $RulesForRole['forum'][1] = $rules['expanded_access'];
            $RulesForRole['forum'][2] = $rules['expanded_access'];
            $RulesForRole['forum'][99] = $rules['no_access'];
			//app dkz. Part media
            $RulesForRole['media'][0] = $rules['full_access'];
            $RulesForRole['media'][1] = $rules['expanded_access'];
            $RulesForRole['media'][2] = $rules['expanded_access'];
            $RulesForRole['media'][99] = $rules['no_access'];
        	//app dkz. Part shopping
            $RulesForRole['shopping'][0] = $rules['full_access'];
            $RulesForRole['shopping'][1] = $rules['expanded_access'];
            $RulesForRole['shopping'][2] = $rules['expanded_access'];
            $RulesForRole['shopping'][99] = $rules['no_access'];
        	//app dkz. Part customs
            $RulesForRole['customs'][0] = $rules['full_access'];
            $RulesForRole['customs'][1] = $rules['expanded_access'];
            $RulesForRole['customs'][2] = $rules['expanded_access'];
            $RulesForRole['customs'][99] = $rules['no_access'];
         	//app dkz. Part transport
            $RulesForRole['transport'][0] = $rules['full_access'];
            $RulesForRole['transport'][1] = $rules['expanded_access'];
            $RulesForRole['transport'][2] = $rules['expanded_access'];
            $RulesForRole['transport'][99] = $rules['no_access'];
            //app dkz. Part office
            $RulesForRole['office'][0] = $rules['full_access'];
            $RulesForRole['office'][1] = $rules['expanded_access'];
            $RulesForRole['office'][2] = $rules['expanded_access'];
            $RulesForRole['office'][99] = $rules['no_access'];
			//app dkz. Part message
            $RulesForRole['message'][0] = $rules['full_access'];
            $RulesForRole['message'][1] = $rules['expanded_access'];
            $RulesForRole['message'][2] = $rules['expanded_access'];
            $RulesForRole['message'][99] = $rules['no_access'];
			
			//app dkz. Part partners
            $RulesForRole['partners'][0] = $rules['full_access'];
            $RulesForRole['partners'][1] = $rules['expanded_access'];
            $RulesForRole['partners'][2] = $rules['expanded_access'];
            $RulesForRole['partners'][99] = $rules['no_access'];
			
            //app dkz. Part settings
            $RulesForRole['settings'][0] = $rules['full_access'];
            $RulesForRole['settings'][1] = $rules['expanded_access'];
            $RulesForRole['settings'][2] = $rules['expanded_access'];
            $RulesForRole['settings'][99] = $rules['no_access'];
           //app dkz. Part notifications
            $RulesForRole['notifications'][0] = $rules['full_access'];
            $RulesForRole['notifications'][1] = $rules['expanded_access'];
            $RulesForRole['notifications'][2] = $rules['expanded_access'];
            $RulesForRole['notifications'][99] = $rules['no_access'];
            


            return $RulesForRole;
        } catch (Exception $e) {
            $e->replyLog($e->getMessage(), $e->getCode());
        }
    }
}
