<?
define ( 'ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] );
define ( 'CORE_DIR', ROOT_DIR . '/core' );
define ( 'DATA_DIR', ROOT_DIR . '/core/data' );
define ( 'CACHE_DIR', ROOT_DIR . '/core/temp/cache/' );

require_once( CORE_DIR . '/data/phpsql.php');
require_once(CORE_DIR.'/libs/class.coreLog.php');
require_once(CORE_DIR.'/libs/class.Cache.php');
require_once(CORE_DIR.'/libs/cache/file.php');

if(!isset($_POST['ajaxData'])) die();
$db = new phpsql;
$func = $_POST['dataType'];
$arg = $_POST['arg'];
$func($arg);


function message_counter($arg){
	//$cache = new Cache('file');
	//$counter = $cache->get('message_counter');
	//echo json_encode($counter[$uid]);
	//exit();
	global $db;
	$uid = (int)$arg['uid'];
	$counter = array($uid=>array('m_cnt'=>0,'n_cnt'=>0));
	$sql = "SELECT  FROM `messages` WHERE ";
	$sqlData['table'] = "messages";
	$sqlData['column'] = " count(`message`) as cnt, `to_user` ";
	$sqlData['where'] =  " `status`=1 and `to_user`=".(int)$uid." group by to_user ";
	$Data = $db->get($sqlData);

	if(count($Data['arr'])>0){
		foreach($Data['arr'] as $item){
			$counter[$item['to_user']]['m_cnt']=$item['cnt'];
		}
	}
	$sqlData['table'] = "( SELECT COUNT(id) AS cnt 
					FROM user_follower_events where 
					follower_id=".(int)$uid."  
					and status=1 
					UNION 
					SELECT COUNT(id) AS cnt 
					FROM user_follower_votes where 
					follower_id=".(int)$uid." 
					and status=1 
					UNION 
					SELECT COUNT(id) AS cnt 
					FROM user_follower_comments where 
					follower_id=".(int)$uid."  and status=1 ) T1 	
			";
		$sqlData['column'] = " SUM( T1.cnt ) as total_cnt ";
		$sqlData['where'] =  " 1 ";
	$Data = $db->get($sqlData,false);
	if(count($Data['arr'])>0){
		foreach($Data['arr'] as $item){
			$counter[$uid]['n_cnt']=$item['total_cnt'];
		}
	}
	echo json_encode(array('m_cnt'=>$counter[$uid]['m_cnt'],'n_cnt'=>$counter[$uid]['n_cnt']));
	exit();
}		
?>